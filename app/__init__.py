"""Application Factory and constructor for the application package."""

import logging
import os
import warnings
from logging.handlers import RotatingFileHandler

from apispec.ext.marshmallow import MarshmallowPlugin
from apispec_webframeworks.flask import FlaskPlugin
from flasgger import APISpec, Swagger, NO_SANITIZER
from flask import Flask
from flask.logging import default_handler
from flask_cors import CORS
from flask_jwt_extended import JWTManager
from flask_marshmallow import Marshmallow
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy

from app.utils import cli
from config import config

db = SQLAlchemy()  # create SQLAlchemy object
ma = Marshmallow()  # create Flask-Marshmallow object
migrate = Migrate()  # create Flask-Migrate object
jwt_manager = JWTManager()  # create JWTManager object


def create_app(config_name):
    """Application factory.

    Args:
        config_name (str): Name of a configuration to use for the application.

    Returns:
        flask app: Flask application instance.

    """
    app = Flask(__name__)
    app.config.from_object(config[config_name])

    # register custom commands
    cli.register(app)

    db.init_app(app)  # initialize SQLAlchemy
    ma.init_app(app)  # initialize Flask-Marshmallow
    migrate.init_app(app, db)  # initialize Flask-Migrate
    jwt_manager.init_app(app)  # initialize JWTManager

    # register blueprint - avoid circular dependencies
    from app.api.v1 import api as api_v1
    app.register_blueprint(api_v1, url_prefix='/api/v1')

    # enable CORS and cache CORS preflight requests for 10 minutes
    CORS(app, resources={r"/api/*": {"origins": "*"}}, max_age=600)

    # missing config in production environment
    if not app.testing and not app.debug:  # pragma: no cover
        if 'SECRET_KEY' not in os.environ:
            warnings.warn('Never use the default secret key in production!', UserWarning)

    # setup logging
    if not app.testing:  # pragma: no cover
        if app.config['LOG_TO_FILE']:
            # create logs directory, if it does not exist
            if not os.path.exists('logs'):
                os.mkdir('logs')

            # create RotatingFileHandler, which saves the last ten log files
            file_handler = RotatingFileHandler('logs/priori.log', maxBytes=10240, backupCount=10)
            file_handler.setFormatter(logging.Formatter('%(asctime)s %(levelname)s: %(message)s '
                                                        '[in %(pathname)s:%(lineno)d]'))
            file_handler.setLevel(logging.WARNING)
            app.logger.addHandler(file_handler)
            app.logger.removeHandler(default_handler)

    # OpenAPI spec
    info = {'description': 'PRIvacy cOmpliant Research Interface. The appropriate JWT can be ' +
            'set via the lock symbol.',
            'contact': {'name': 'Moritz Leitner', 'email': 'info@moritzleitner.net'}}
    spec = APISpec(title='PRIORI', info=info, version='1.0', openapi_version='3.0.3',
                   plugins=[FlaskPlugin(), MarshmallowPlugin()], security=[{'JWT': []}])
    spec.components.security_scheme('JWT', {"type": "http", "scheme": "bearer",
                                            "bearerFormat": "JWT"})

    # setup Flasgger using template from APISpec
    from app.models.schemas import (UserSchema, ProjectSchema, DatasetSchema,
                                    HierarchyGenerationSchema)
    template = spec.to_flasgger(app, definitions=[UserSchema, ProjectSchema, DatasetSchema,
                                                  HierarchyGenerationSchema])
    Swagger(app, template=template, sanitizer=NO_SANITIZER)

    return app
