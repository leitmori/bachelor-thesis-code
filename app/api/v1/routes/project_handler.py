"""This module contains the project_handler routes and some helper functions."""

import os
import uuid

import pandas as pd
from flask import current_app, jsonify, request, send_from_directory, url_for
from flask_jwt_extended import get_jwt_claims, get_jwt_identity, jwt_required
from opendp.whitenoise.metadata.collection import (Boolean, CollectionMetadata, DateTime, Float,
                                                   Int, String, Table)
from opendp.whitenoise.sql import PandasReader, PrivateReader
from werkzeug.utils import secure_filename

from app.api.v1 import api
from app.api.v1.authenticator.helpers import privileged_user_required
from app.api.v1.errors import BadRequest, Forbidden, NotFound
from app.api.v1.routes.dataset_handler import anonymize_dataset
from app.api.v1.routes.helper import add_prev_next, paginate
from app.models.models import Dataset, Project
from app.models.schemas import Attribute, DataType, ProjectSchema

# project_schema schemas to use in routes
project_schema = ProjectSchema()


def create_metadata(attributes, num_rows):
    """Create metadata object for differentially private queries from given attributes.

    Args:
        attributes (dict): Attributes of the dataset in AttributeSchema.
        num_rows (int): Number of rows in the dataset.

    Returns:
        opendp.whitenoise.metadata.collection.CollectionMetadata: Metadata object describing the
            dataset structure.

    """
    # create columns
    columns = []
    for attribute in attributes:
        if not attribute['attribute_type'] == Attribute.IDENTIFYING:
            if attribute['metadata']['datatype'] == DataType.BOOL:
                columns.append(Boolean(attribute['attribute_name']))
            elif attribute['metadata']['datatype'] == DataType.DATETIME:
                columns.append(DateTime(attribute['attribute_name']))
            elif attribute['metadata']['datatype'] == DataType.FLOAT:
                columns.append(Float(attribute['attribute_name'], attribute['metadata']['min'],
                                     attribute['metadata']['max']))
            elif attribute['metadata']['datatype'] == DataType.INT:
                minimum = int(attribute['metadata']['min']
                              ) if attribute['metadata']['min'] else None
                maximum = int(attribute['metadata']['max']
                              ) if attribute['metadata']['max'] else None
                columns.append(Int(attribute['attribute_name'], minimum, maximum))
            elif attribute['metadata']['datatype'] == DataType.STR:
                columns.append(String(attribute['attribute_name'],
                                      attribute['metadata']['cardinality']))

    # add ID column
    columns.append(Int("ID", is_key=True))

    meta = Table('', 'PRIORI', num_rows, columns)
    return CollectionMetadata([meta], "csv")


def execute_query(project, df, query):
    """Execute a differentially private query on the dataframe.

    Args:
        project (Project): Project to which the dataframe belongs.
        df (pandas.DataFrame): Dataframe on which the query is executed.
        query (str): Query string.

    Raises:
        BadRequest: Attribute specified in the query does not exist.

    Returns:
        list: Result of query.

    """
    try:
        epsilon = project.config['privacy_models'][0]['model_params']['epsilon']
        delta = project.config['privacy_models'][0]['model_params']['delta']
        metadata = create_metadata(project.config['attributes'], df.shape[0])
        reader = PandasReader(metadata, df)
        private_reader = PrivateReader(metadata, reader, epsilon, delta)
        return private_reader.execute(query)
    except ValueError as e:
        raise BadRequest(str(e))
    except KeyError as e:
        msg = str(e)
        raise BadRequest("Could not find attribute with name '" +
                         msg[msg.find("(") + 1:msg.find(")")] + "'")


@api.route('/projects', methods=['GET'])
@jwt_required
def get_projects():
    """Returns a list of all projects (paginated).
    If the user is not privileged (researcher), he will see only the projects being shared with him.
    For projects using differential privacy, the metadata of the attributes are returned so \
    that the researcher can make reasonable queries.
    Access token has to be set in the Authorization header using the Bearer schema.
    ---
    tags:
      - Projects
    parameters:
      - in: query
        name: page
        schema:
          type: integer
        description: Requested page of projects. Defaults to 1 if no such query parameter.
      - in: query
        name: limit
        schema:
          type: integer
        description: Requested number of projects. Defaults to 20 if no such query parameter.
    responses:
      200:
        description: OK. ```researcher_id``` will not be in the response (redundant).
        content:
          application/json:
            schema:
              type: array
              items:
                $ref: '#/components/schemas/Project'
      401:
        description: Authorization Header is missing or token has been revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Missing Authorization Header
                    - Token has been revoked
    """
    claims = get_jwt_claims()
    if claims['privileged']:
        projects = Project.query
    else:
        projects = Project.query.filter_by(researcher_id=get_jwt_identity())

    # paginate query and dump paginated items
    pagination = paginate(request, projects)
    response_data = project_schema.dump(pagination.items, many=True)

    # add previous and next URLs
    response_data = add_prev_next('api.get_projects', {'projects': response_data}, pagination)
    return jsonify(response_data), 200


@api.route('/projects', methods=['POST'])
@privileged_user_required
def create_project():
    """Creates a new project. Privileged user required.
    In this way, a privileged user can share a dataset with a non-privileged user in anonymized \
    form or enable the non-privileged user to perform differentially private queries. <br>
    Non-interactive mechanisms: Identifying attributes (e.g. health insurance number) allow a \
    re-identification of the affected persons and are therefore redacted. Quasi-identifying \
    attributes (e.g. zip code or gender) pose a threat together with external information and \
    are therefore transformed, for example with k-anonymity. Sensitive attributes (e.g. medical \
    diagnoses) should not be disclosed to the public and can be protected with t-closeness or \
    l-diversity. Non-sensitive attributes remain unchanged. <br>
    Interactive mechanisms: Identifying attributes will be completely deleted if differential \
    privacy is used. Differentially private processing needs to know which attributes can be used \
    in numeric computations. Metadata should not be data-dependent. For example, the acceptable \
    range for the age column should be domain-specific, and should not use the actual minimum and \
    maximum values from the data.
    Example of a valid request:
    ```
    {
      "name": "Heart Rate",
      "description": "Heart Rate Data",
      "researcher_id": 1,
      "dataset_id": 2,
      "config": {
        "attributes": [
          {
            "attribute_name": "age",
            "attribute_type": "insensitive",
            "metadata": {
              "datatype": "int",
              "min": 0,
              "max": 90
            }
          },
          {
            "attribute_name": "gender",
            "attribute_type": "insensitive",
            "metadata": {
              "datatype": "str",
              "cardinality": 2
            }
          }
        ],
        "privacy_models": [
          {
            "model_name": "epsilon-delta-differential_privacy",
            "model_params": {
              "epsilon": 2,
              "delta": 10E-16
            }
          }
        ]
      }
    }
    ```
    ---
    tags:
      - Projects
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Project'

    responses:
      201:
        description: Project has been successfully created.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Project'
      400:
        description: Bad Request
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Bad Request
                msg:
                  type: string
                  enum:
                    - Missing JSON.
      401:
        description: Authorization Header is missing or token has been revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Missing Authorization Header
                    - Token has been revoked
      403:
        description: User is not privileged.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Forbidden
                msg:
                  type: string
                  enum:
                    - Privileged users only!
      500:
        description: Internal Server Error.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Internal Server Error
                msg:
                  type: string
    """
    if not request.json:
        raise BadRequest('Missing JSON.')

    project = project_schema.load(request.json)
    project.creator_id = get_jwt_identity()
    dataset = Dataset.query.get(project.dataset_id)

    if project.uses_dp():
        # read csv
        df = pd.read_csv(os.path.join(
            current_app.config['UPLOAD_FOLDER'], dataset.filename),
            sep=None, engine='python')

        if len(df.columns) != len(project.config['attributes']):
            raise BadRequest('Properties must be defined for each attribute.')

        # drop identifying attributes
        for attribute in project.config['attributes']:
            if attribute['attribute_type'] == Attribute.IDENTIFYING:
                df.drop(attribute['attribute_name'], axis=1, inplace=True)

        # insert ID column
        df.insert(0, 'ID', range(0, len(df)))

        # check with a query if metadata are not completely wrong
        query = 'SELECT COUNT(*) FROM PRIORI'
        execute_query(project, df, query)
        result = df
    else:
        # anonymize dataset with given config
        result = anonymize_dataset(project.config, dataset).dataset.to_dataframe()

    # create project folder if it does not exist
    if not os.path.exists(current_app.config['PROJECT_FOLDER']):
        os.mkdir(current_app.config['PROJECT_FOLDER'])

    # store project csv
    filename = str(uuid.uuid4()) + '.csv'
    result.to_csv(os.path.join(current_app.config['PROJECT_FOLDER'], filename), index=False)

    # store project in db
    project.filename = filename
    project.save()

    response = project_schema.jsonify(project)

    # add url to created dataset to response
    url = url_for('api.get_project', project_id=project.project_id)
    return response, 201, {'Location': url}


@api.route('/projects/<int:project_id>', methods=['GET'])
@jwt_required
def get_project(project_id):
    """Returns project with given project_id.
    If the user is not privileged (researcher), he can only access projects being shared with him.
    For projects using differential privacy, the metadata of the attributes are returned so \
    that the researcher can make reasonable queries.
    Access token has to be set in the Authorization header using the Bearer schema.
    ---
    tags:
      - Projects
    parameters:
      - in: path
        name: project_id
        schema:
          type: integer
        required: true
        description: ID of the project to get
    responses:
      200:
        description: OK. ```researcher_id``` will not be in the response (redundant).
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Project'
      401:
        description: Authorization Header is missing or token has been revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Missing Authorization Header
                    - Token has been revoked
      403:
        description: User may not access the project
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Forbidden
                msg:
                  type: string
                  enum:
                    - Access denied.
      404:
        description: No project with the ```project_id``` could be found.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Not Found
                msg:
                  type: string
                  enum:
                    - Project not found.
    """
    claims = get_jwt_claims()
    project = Project.query.get(project_id)

    # raise NotFound error if dataset does not exist
    if project is None:
        raise NotFound('Project not found.')

    # check access right
    if not claims['privileged']:
        if not project.researcher_id == get_jwt_identity():
            raise Forbidden('Access denied.')

    return project_schema.jsonify(project), 200


@api.route('/projects/<int:project_id>', methods=['DELETE'])
@privileged_user_required
def delete_project(project_id):
    """Deletes project with given project_id. Privileged user required.
    Refresh token has to be set in the Authorization header using the Bearer schema.
    ---
    tags:
      - Projects
    parameters:
      - in: path
        name: project_id
        schema:
          type: integer
        required: true
        description: ID of the project to delete
    responses:
      200:
        description: Project has been successfully deleted.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Project successfully deleted.
      401:
        description: Authorization Header is missing or token has been revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Missing Authorization Header
                    - Token has been revoked
      403:
        description: User is not privileged.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Forbidden
                msg:
                  type: string
                  enum:
                    - Privileged users only!
      404:
        description: No project with the ```project_id``` could be found.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Not Found
                msg:
                  type: string
                  enum:
                    - Project not found.
    """
    project = Project.query.get(project_id)

    # raise NotFound error if dataset does not exist
    if project is None:
        raise NotFound('Project not found.')

    # delete csv
    path = os.path.join(current_app.config['PROJECT_FOLDER'], project.filename)
    if os.path.exists(path):
        os.remove(path)

    # delete project in db
    project.delete()

    return jsonify({"msg": "Project successfully deleted."}), 200


@api.route('/projects/<int:project_id>/csv', methods=['GET'])
@jwt_required
def get_csv(project_id):
    """Returns the anonymized dataset as CSV. Works only for projects that do not use differential \
    privacy.
    If the user is not privileged (researcher), he can only access projects being shared with him.
    Access token has to be set in the Authorization header using the Bearer schema.
    ---
    tags:
      - Projects
    parameters:
      - in: path
        name: project_id
        schema:
          type: integer
        required: true
        description: ID of the project
    responses:
      200:
        description: OK.
        content:
          text/csv:
            schema:
              type: string
              format: binary
      400:
        description: Bad Request
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Bad Request
                msg:
                  type: string
                  enum:
                    - This route is not intended for projects using differential privacy.
      401:
        description: Authorization Header is missing or token has been revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Missing Authorization Header
                    - Token has been revoked
      403:
        description: User may not access the project
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Forbidden
                msg:
                  type: string
                  enum:
                    - Access denied.
      404:
        description: No project with the ```project_id``` could be found.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Not Found
                msg:
                  type: string
                  enum:
                    - Project not found.
    """
    claims = get_jwt_claims()
    project = Project.query.get(project_id)

    # raise NotFound error if dataset does not exist
    if project is None:
        raise NotFound('Project not found.')

    # check access right
    if not claims['privileged']:
        if not project.researcher_id == get_jwt_identity():
            raise Forbidden('Access denied.')

    if project.uses_dp():
        raise BadRequest('This route is not intended for projects using differential privacy.')

    csv_name = secure_filename(project.name + ".csv")
    path = os.path.join(os.path.abspath(os.getcwd()), current_app.config['PROJECT_FOLDER'])
    return send_from_directory(path, project.filename, as_attachment=True,
                               attachment_filename=csv_name)


@api.route('/projects/<int:project_id>/analyze', methods=['POST'])
@jwt_required
def analyze_differentially_private(project_id):
    """Returns the differentially private result for the given query. Works only for projects \
    that use differential privacy.
    Use PRIORI as table name in your SQL-Query. Supports COUNT, SUM, AVG, VARIANCE, STDDEV and \
    GROUP BY.
    If the user is not privileged (researcher), he can only access projects being shared with him.
    Access token has to be set in the Authorization header using the Bearer schema.
    ---
    tags:
      - Projects
    parameters:
      - in: path
        name: project_id
        schema:
          type: integer
        required: true
        description: ID of the project
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              query:
                type: string
                example: SELECT COUNT(*) FROM PRIORI
            required:
              - query
    responses:
      200:
        description: OK.
        content:
          application/json:
            schema:
              properties:
                query_result:
                  type: string
      400:
        description: Bad Request
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Bad Request
                msg:
                  type: string
                  enum:
                    - This route is intended exclusively for differential privacy.
      401:
        description: Authorization Header is missing or token has been revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Missing Authorization Header
                    - Token has been revoked
      403:
        description: User may not access the project
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Forbidden
                msg:
                  type: string
                  enum:
                    - Access denied.
      404:
        description: No project with the ```project_id``` could be found.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Not Found
                msg:
                  type: string
                  enum:
                    - Project not found.
    """
    if not request.json:
        raise BadRequest('Missing JSON.')
    if 'query' not in request.json:
        raise BadRequest('Missing data for required field.')
    query = request.json['query']
    if not isinstance(query, str):
        raise BadRequest('Query must be of type str.')

    claims = get_jwt_claims()
    project = Project.query.get(project_id)

    # raise NotFound error if dataset does not exist
    if project is None:
        raise NotFound('Project not found.')

    # check access right
    if not claims['privileged']:
        if not project.researcher_id == get_jwt_identity():
            raise Forbidden('Access denied.')

    if not project.uses_dp():
        raise BadRequest('This route is intended exclusively for differential privacy.')

    # read csv
    df = pd.read_csv(os.path.join(
        current_app.config['PROJECT_FOLDER'], project.filename), sep=None, engine='python')

    # execute query
    result = execute_query(project, df, query)

    result_df = pd.DataFrame(result[1:], columns=result[0])
    return jsonify(query_result=result_df.to_dict()), 200
