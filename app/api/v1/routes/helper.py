"""This module contains helper methods for the routes module."""

from flask import url_for

# default value of number of items per page
LIMIT = 20


def paginate(request, query):
    """Get pagination object of given query according to request parameters.

    Args:
        request (Request): A Request object with parameters page (default: 1), limit (default: 20).
        query (BaseQuery): A Query object to paginate.

    Returns:
        Pagination: Pagination of query.

    """
    # get page number and limit from request (default: page=1, limit=20)
    page = request.args.get('page', 1, type=int)
    limit = request.args.get('limit', LIMIT, type=int)
    pagination = query.paginate(page, limit, False)
    return pagination


def add_prev_next(url, data, page, **kwargs):
    """Add URLs for next and previous page to given data.

    Args:
        url (str): URL of current (Flask) endpoint.
        data (dic of str : list) Dictionary to which URLs are added.
        page (Pagination): Pagination object of current page.
        kwargs (dic of str : obj) Other needed arguments for the endpoint.

    Returns:
        dic of str: obj: Contains data and URLs for previous and next page.
                         Value of previous/next ist None if previous/next page does not exist.

    """
    # set URL to previous/next page if they exist (else: None)
    if page.has_next:
        next_url = url_for(url, page=page.next_num, limit=page.per_page, **kwargs)
    else:
        next_url = None
    if page.has_prev:
        prev_url = url_for(url, page=page.prev_num, limit=page.per_page, **kwargs)
    else:
        prev_url = None
    data['previous'] = prev_url
    data['next'] = next_url
    return data


def clean_empty(d):
    """Remove all empty fields in a nested dict.

    Args:
        d (dict): Input dict.

    Returns:
        dict: Output dict.

    """
    if not isinstance(d, (dict, list)):
        return d
    if isinstance(d, list):
        return [v for v in (clean_empty(v) for v in d) if v or v == 0]
    return {k: v for k, v in ((k, clean_empty(v)) for k, v in d.items()) if v or v == 0}
