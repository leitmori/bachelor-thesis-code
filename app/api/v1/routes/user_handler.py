"""This module contains the user_handler routes."""

from flask import jsonify, request, url_for

from app.api.v1 import api
from app.api.v1.authenticator.helpers import privileged_user_required
from app.api.v1.errors import BadRequest, NotFound
from app.api.v1.routes.helper import add_prev_next, paginate
from app.models.models import User
from app.models.schemas import UserSchema

# global user schema to use in routes
user_schema = UserSchema()


@api.route('/users', methods=['GET'])
@privileged_user_required
def get_users():
    """Returns a list of all users (paginated). Privileged user required.
    Access token has to be set in the Authorization header using the Bearer schema.
    ---
    tags:
      - Users
    parameters:
      - in: query
        name: page
        schema:
          type: integer
        description: Requested page of users. Defaults to 1 if no such query parameter.
      - in: query
        name: limit
        schema:
          type: integer
        description: Requested number of users. Defaults to 20 if no such query parameter.
    responses:
      200:
        description: OK
        content:
          application/json:
            schema:
              type: array
              items:
                $ref: '#/components/schemas/User'
      401:
        description: Authorization Header is missing or token has been revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Missing Authorization Header
                    - Token has been revoked
      403:
        description: User is not privileged.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Forbidden
                msg:
                  type: string
                  enum:
                    - Privileged users only!
    """
    # paginate query and dump paginated items
    pagination = paginate(request, User.query)
    response_data = user_schema.dump(pagination.items, many=True)

    # add previous and next URLs
    response_data = add_prev_next('api.get_users', {'users': response_data}, pagination)
    return jsonify(response_data), 200


@api.route('/users', methods=['POST'])
@privileged_user_required
def create_user():
    """Creates a new user. Privileged user required.
    Access token has to be set in the Authorization header using the Bearer schema.
    ---
    tags:
      - Users
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/User'
    responses:
      201:
        description: User has been successfully created.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'
      400:
        description: Bad Request
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Bad Request
                msg:
                  type: string
                  enum:
                    - Missing JSON.
      401:
        description: Authorization Header is missing or token has been revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Missing Authorization Header
                    - Token has been revoked
      403:
        description: User is not privileged.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Forbidden
                msg:
                  type: string
                  enum:
                    - Privileged users only!
    """
    if not request.json:
        raise BadRequest('Missing JSON.')

    user = user_schema.load(request.json)
    user.save()
    response = user_schema.jsonify(user)

    # add url to created user to response
    url = url_for('api.get_user', user_id=user.user_id)
    return response, 201, {'Location': url}


@api.route('/users/<int:user_id>', methods=['GET'])
@privileged_user_required
def get_user(user_id):
    """Returns user with given user_id. Privileged user required.
    Access token has to be set in the Authorization header using the Bearer schema.
    ---
    tags:
      - Users
    parameters:
      - in: path
        name: user_id
        schema:
          type: integer
        required: true
        description: ID of the user to get
    responses:
      200:
        description: OK
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/User'
      401:
        description: Authorization Header is missing or token has been revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Missing Authorization Header
                    - Token has been revoked
      403:
        description: User is not privileged.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Forbidden
                msg:
                  type: string
                  enum:
                    - Privileged users only!
      404:
        description: No user with the ```user_id``` could be found.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Not Found
                msg:
                  type: string
                  enum:
                    - User not found.
    """
    user = User.query.get(user_id)

    # raise NotFound error if user does not exist
    if user is None:
        raise NotFound('User not found.')

    return user_schema.jsonify(user), 200
