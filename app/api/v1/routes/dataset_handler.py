"""This module contains the dataset_handler routes and some helper functions."""

import json
import os
import uuid

import pandas as pd
import requests
from flask import current_app, jsonify, request, url_for
from flask_jwt_extended import get_jwt_identity
from pyarxaas import ARXaaS, AttributeType
from pyarxaas import Dataset as arx_ds
from pyarxaas import privacy_models
from pyarxaas.hierarchy import IntervalHierarchyBuilder, RedactionHierarchyBuilder
from werkzeug.utils import secure_filename

from app.api.v1 import api
from app.api.v1.authenticator.helpers import privileged_user_required
from app.api.v1.errors import BadRequest, InternalServerError, NotFound
from app.api.v1.routes.helper import add_prev_next, paginate
from app.models.models import Dataset
from app.models.schemas import (Attribute, AttributeGeneralizationSchema, ConfigSchema,
                                DatasetSchema, HierarchyGenerationSchema, HierarchyType,
                                PrivacyModel, RedactionOrder, RiskIntervalSchema)

# global schemas to use in routes
dataset_schema = DatasetSchema()
config_schema = ConfigSchema()
attribute_generalization_schema = AttributeGeneralizationSchema()
risk_interval_schema = RiskIntervalSchema()
hierarchy_generation_schema = HierarchyGenerationSchema()


def is_extension_allowed(filename):
    """Check if file extension is allowed.

    Args:
        filename (str): Filename to check.

    Returns:
        bool: True -> Extension allowed, False -> Extension forbidden.

    """
    if '.' in filename:
        if filename.rsplit('.', 1)[1].lower() in current_app.config['ALLOWED_EXTENSIONS']:
            return True
    return False


def anonymize_dataset(config, dataset):
    """Anonymize the given dataset with the desired config.

    Args:
        config (dict): Contains the configuration for attributes (e.g. hierarchies) and the
            configuration for privacy models as well as the suppression_limit.
        dataset (Dataset): The dataset (as database record) which should be anonymized
            with the given config.

    Raises:
        BadRequest: Invalid params.
        InternalServerError: If ARXaaS could not be reached or a severe error occurred.

    Returns:
        pyarxaas.models.anonymize_result.AnonymizeResult: The result of the anonymization
            using ARXaaS.

    """
    # read csv
    df = pd.read_csv(os.path.join(current_app.config['UPLOAD_FOLDER'],
                                  dataset.filename), sep=None, engine='python')
    ds = arx_ds.from_pandas(df)

    # apply attribute config
    try:
        for attribute in config['attributes']:
            if attribute['attribute_type'] == Attribute.IDENTIFYING:
                ds.set_attribute_type(AttributeType.IDENTIFYING, attribute['attribute_name'])
            elif attribute['attribute_type'] == Attribute.QUASIIDENTIFYING:
                ds.set_attribute_type(AttributeType.QUASIIDENTIFYING, attribute['attribute_name'])
            elif attribute['attribute_type'] == Attribute.SENSITIVE:
                ds.set_attribute_type(AttributeType.SENSITIVE, attribute['attribute_name'])
            elif attribute['attribute_type'] == Attribute.INSENSITIVE:
                ds.set_attribute_type(AttributeType.INSENSITIVE, attribute['attribute_name'])

            if 'attribute_hierarchy' in attribute and attribute['attribute_hierarchy'] is not None:
                ds.set_hierarchy(attribute['attribute_name'], attribute['attribute_hierarchy'])
    except KeyError as e:
        msg = str(e)
        raise BadRequest("Could not find attribute with name '"
                         + msg[msg.find("(") + 1:msg.find(")")] + "'")

    # create privacy model objects with desired params
    models = []
    for p_m in config['privacy_models']:
        if p_m['model_name'] == PrivacyModel.K_ANONYMITY:
            models.append(privacy_models.KAnonymity(k=p_m['model_params']['k']))
        elif p_m['model_name'] == PrivacyModel.L_DIVERSITY_DISTINCT:
            models.append(privacy_models.LDiversityDistinct(
                l=p_m['model_params']['l'], column_name=p_m['attribute_name']))
        elif p_m['model_name'] == PrivacyModel.L_DIVERSITY_GRASSBERGER_ENTROPY:
            models.append(privacy_models.LDiversityGrassbergerEntropy(
                l=p_m['model_params']['l'], column_name=p_m['attribute_name']))
        elif p_m['model_name'] == PrivacyModel.L_DIVERSITY_RECURSIVE:
            models.append(privacy_models.LDiversityRecursive(
                l=p_m['model_params']['l'], c=p_m['model_params']['c'],
                column_name=p_m['attribute_name']))
        elif p_m['model_name'] == PrivacyModel.L_DIVERSITY_SHANNON_ENTROPY:
            models.append(privacy_models.LDiversityShannonEntropy(
                l=p_m['model_params']['l'], column_name=p_m['attribute_name']))
        elif p_m['model_name'] == PrivacyModel.T_CLOSENESS_EQUAL_DISTANCE:
            models.append(privacy_models.TClosenessEqualDistance(
                t=p_m['model_params']['t'], column_name=p_m['attribute_name']))
        elif p_m['model_name'] == PrivacyModel.T_CLOSENESS_ORDERED_DISTANCE:
            models.append(privacy_models.TClosenessOrderedDistance(
                t=p_m['model_params']['t'], column_name=p_m['attribute_name']))
        else:
            raise BadRequest('Privacy model not supported for anonymization.')

    # apply privacy models with given suppression_limit
    try:
        arxaas = ARXaaS(current_app.config['ARXAAS_URL'])
        return arxaas.anonymize(dataset=ds, privacy_models=models,
                                suppression_limit=config['suppression_limit'])

    # handle some possible errors
    except requests.exceptions.ConnectionError:
        raise InternalServerError(
            'Could not reach ARXaaS. Please check the ARXAAS_URL in config.py.')
    except requests.exceptions.RequestException as e:
        try:
            msg = repr(e).strip().replace('\\', '')
            message = json.loads(msg[msg.find('(') + 2:msg.find("')")])['message']
        except Exception:
            raise e
        raise BadRequest(message)
    except Exception as e:
        raise InternalServerError('Something bad happened:' + repr(e))


def create_analyzation_response(result):
    """Create dict containing the analysis of the anonymized dataset.

    Args:
        result (pyarxaas.models.anonymize_result.AnonymizeResult): The result of the anonymization
            using ARXaaS.

    Returns:
        dict: Dict containing the analysis of the anonymized dataset.

    """
    attacker_success_rate = {
        k.lower(): v for k, v in result.risk_profile.attacker_success_rate.items()}
    distribution_of_risk = risk_interval_schema.load(
        result.risk_profile.distribution_of_risk['riskIntervalList'], many=True)
    risk_profile = {'attacker_success_rate': attacker_success_rate,
                    'distribution_of_risk': distribution_of_risk,
                    're_identification_risk': result.risk_profile.re_identification_risk}
    attribute_generalization = attribute_generalization_schema.load(
        result.anonymization_metrics.attribute_generalization, many=True)

    return {'attribute_generalization': attribute_generalization, 'risk_profile': risk_profile}


def create_hierarchy(hierarchy_request, dataset):
    """Create a value generalization hierarchy.

    Args:
        hierarchy_request (dict): Desired hierarchy in hierarchy_generation_schema.
        dataset (Dataset): The dataset (as database record) for which the hierarchy is to be
            created.

    Raises:
        InternalServerError: If ARXaaS could not be reached or a severe error occurred.
        BadRequest: Invalid params.

    Returns:
        list[list]: The created hierarchy.
    """
    # read csv
    df = pd.read_csv(os.path.join(current_app.config['UPLOAD_FOLDER'],
                                  dataset.filename), sep=None, engine='python')
    try:
        arxaas = ARXaaS(current_app.config['ARXAAS_URL'])

        # create redaction hierarchy
        if hierarchy_request['transformation_type'] == HierarchyType.REDACTION:
            if hierarchy_request['redaction_order'] == RedactionOrder.LEFT_TO_RIGHT:
                builder = RedactionHierarchyBuilder(
                    redaction_order=RedactionHierarchyBuilder.Order.LEFT_TO_RIGHT)
            elif hierarchy_request['redaction_order'] == RedactionOrder.RIGHT_TO_LEFT:
                builder = RedactionHierarchyBuilder(
                    redaction_order=RedactionHierarchyBuilder.Order.RIGHT_TO_LEFT)

        # create interval hierarchy
        elif hierarchy_request['transformation_type'] == HierarchyType.INTERVAL:
            builder = IntervalHierarchyBuilder()
            for interval in hierarchy_request['intervals']:
                builder.add_interval(interval['min'], interval['max'], interval['label'])

        return arxaas.hierarchy(builder, df[hierarchy_request['attribute_name']].tolist())

    # handle some possible errors
    except requests.exceptions.ConnectionError:
        raise InternalServerError(
            'Could not reach ARXaaS. Please check the ARXAAS_URL in config.py.')
    except KeyError as e:
        msg = str(e)
        raise BadRequest("Could not find attribute with name " + msg)
    except requests.exceptions.RequestException as e:
        try:
            msg = repr(e).strip().replace('\\', '')
            message = json.loads(msg[msg.find('(') + 2:msg.find("')")])['message']
        except Exception:
            raise e
        raise BadRequest(message)
    except Exception as e:
        raise InternalServerError('Something bad happened:' + repr(e))


@api.route('/datasets', methods=['GET'])
@privileged_user_required
def get_datasets():
    """Returns a list of all datasets (paginated). Privileged user required.
    Access token has to be set in the Authorization header using the Bearer schema.
    ---
    tags:
      - Datasets
    parameters:
      - in: query
        name: page
        schema:
          type: integer
        description: Requested page of datasets. Defaults to 1 if no such query parameter.
      - in: query
        name: limit
        schema:
          type: integer
        description: Requested number of datasets. Defaults to 20 if no such query parameter.
    responses:
      200:
        description: OK
        content:
          application/json:
            schema:
              type: array
              items:
                $ref: '#/components/schemas/Dataset'
      401:
        description: Authorization Header is missing or token has been revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Missing Authorization Header
                    - Token has been revoked
      403:
        description: User is not privileged.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Forbidden
                msg:
                  type: string
                  enum:
                    - Privileged users only!
    """
    # paginate query and dump paginated items
    pagination = paginate(request, Dataset.query)
    response_data = dataset_schema.dump(pagination.items, many=True)

    # add previous and next URLs
    response_data = add_prev_next('api.get_datasets', {'datasets': response_data}, pagination)
    return jsonify(response_data), 200


@api.route('/datasets', methods=['POST'])
@privileged_user_required
def create_dataset():
    """Creates a new dataset. Privileged user required.
    Access token has to be set in the Authorization header using the Bearer schema.
    ---
    tags:
      - Datasets
    requestBody:
      required: true
      content:
        multipart/form-data:
          schema:
            type: object
            properties:
              json:
                type: string
                format: binary
                description: '```name``` and ```description``` of the dataset as a .json-file. \
                  Swagger UI unfortunately still has problems with multipart requests.'
              file:
                type: string
                format: binary
                description: The dataset to upload as CSV.
            required:
              - json
              - file
    responses:
      201:
        description: Dataset has been successfully processed.
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Dataset'
      400:
        description: Bad Request
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Bad Request
                msg:
                  type: string
                  enum:
                    - Missing JSON.
      401:
        description: Authorization Header is missing or token has been revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Missing Authorization Header
                    - Token has been revoked
      403:
        description: User is not privileged.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Forbidden
                msg:
                  type: string
                  enum:
                    - Privileged users only!
    """
    # get JSON data
    if 'json' in request.files:
        json_data = json.load(request.files['json'])
    elif request.json:
        json_data = request.json
    else:
        raise BadRequest('Missing JSON.')

    dataset = dataset_schema.load(json_data)
    dataset.creator_id = get_jwt_identity()

    # get file
    if 'file' in request.files:
        file = request.files['file']
    else:
        raise BadRequest('Missing file.')

    # handle uploaded dataset
    if not is_extension_allowed(file.filename):
        raise BadRequest('File extension not allowed: ' + file.filename)

    # create upload folder if it does not exist
    if not os.path.exists(current_app.config['UPLOAD_FOLDER']):
        os.mkdir(current_app.config['UPLOAD_FOLDER'])

    # save to disk
    filename = "{uuid}${filename}".format(uuid=str(uuid.uuid4()),
                                          filename=secure_filename(file.filename))
    dataset.filename = filename
    dataset.save()
    file.save(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))

    response = dataset_schema.jsonify(dataset)

    # add url to created dataset to response
    url = url_for('api.get_dataset', dataset_id=dataset.dataset_id)
    return response, 201, {'Location': url}


@api.route('/datasets/<int:dataset_id>', methods=['GET'])
@privileged_user_required
def get_dataset(dataset_id):
    """Returns dataset with given dataset_id. Privileged user required.
    Access token has to be set in the Authorization header using the Bearer schema.
    ---
    tags:
      - Datasets
    parameters:
      - in: path
        name: dataset_id
        schema:
          type: integer
        required: true
        description: ID of the dataset to get
    responses:
      200:
        description: OK
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Dataset'
      401:
        description: Authorization Header is missing or token has been revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Missing Authorization Header
                    - Token has been revoked
      403:
        description: User is not privileged.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Forbidden
                msg:
                  type: string
                  enum:
                    - Privileged users only!
      404:
        description: No dataset with the ```dataset_id``` could be found.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Not Found
                msg:
                  type: string
                  enum:
                    - Dataset not found.
    """
    dataset = Dataset.query.get(dataset_id)

    # raise NotFound error if dataset does not exist
    if dataset is None:
        raise NotFound('Dataset not found.')

    return dataset_schema.jsonify(dataset), 200


@api.route('/datasets/<int:dataset_id>/calculate-weighted-risk', methods=['POST'])
@privileged_user_required
def calculate_weighted_risk(dataset_id):
    """Calculates the weighted risk for the given dataset. Privileged user required.
    Identifying attributes (e.g. health insurance number) allow a re-identification of the \
    affected persons and are therefore redacted. Quasi-identifying attributes (e.g. zip code \
    or gender) pose a threat together with external information and are therefore transformed, \
    for example with k-anonymity. Sensitive attributes (e.g. medical diagnoses) should not be \
    disclosed to the public and can be protected with t-closeness or l-diversity. Non-sensitive \
    attributes remain unchanged.
    Access token has to be set in the Authorization header using the Bearer schema.
    ```metadata``` is only needed for differential privacy and must therefore not be specified.
    Example of a valid request:
    ```
    {
      "attributes": [
        {
          "attribute_name": "zipcode",
          "attribute_type": "quasi-identifying",
          "attribute_hierarchy": [
            [
              "81667",
              "8166*",
              "816**",
              "81***",
              "8****",
              "*****"
            ],
            [
              "81668",
              "8166*",
              "816**",
              "81***",
              "8****",
              "*****"
            ]
          ]
        },
        {
          "attribute_name": "age",
          "attribute_type": "quasi-identifying",
          "attribute_hierarchy": [
            [
              "18",
              "young-adult",
              "*"
            ],
            [
              "19",
              "young-adult",
              "*"
            ],
            [
              "20",
              "young-adult",
              "*"
            ]
          ]
        },
        {
          "attribute_name": "disease",
          "attribute_type": "sensitive"
        }
      ],
      "privacy_models": [
        {
          "model_name": "k-anonymity",
          "model_params": {
            "k": 5
          }
        },
        {
          "model_name": "l-diversity_distinct",
          "attribute_name": "disease",
          "model_params": {
            "l": 2
          }
        }
      ],
      "suppression_limit": 0.2
    }
    ```
    ---
    tags:
      - Datasets
    parameters:
      - in: path
        name: dataset_id
        schema:
          type: integer
        required: true
        description: ID of the dataset
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Config'
    responses:
      200:
        description: OK
        content:
          application/json:
            schema:
              properties:
                risk:
                  type: number
                  format: float
                  minimum: 0
                  maximum: 1
      400:
        description: Bad Request
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Bad Request
                msg:
                  type: string
                  enum:
                    - Missing JSON.
      401:
        description: Authorization Header is missing or token has been revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Missing Authorization Header
                    - Token has been revoked
      403:
        description: User is not privileged.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Forbidden
                msg:
                  type: string
                  enum:
                    - Privileged users only!
      404:
        description: No dataset with the ```dataset_id``` could be found.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Not Found
                msg:
                  type: string
                  enum:
                    - Dataset not found.
      500:
        description: Internal Server Error.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Internal Server Error
                msg:
                  type: string
    """
    dataset = Dataset.query.get(dataset_id)

    # raise NotFound error if dataset does not exist
    if dataset is None:
        raise NotFound('Dataset not found.')

    if not request.json:
        raise BadRequest('Missing JSON.')

    # anonymize dataset with given config
    config = config_schema.load(request.json)
    anonymized_dataset = anonymize_dataset(config, dataset)
    re_identification_risk = anonymized_dataset.risk_profile.re_identification_risk
    weights = current_app.config['WEIGHTED_RISK']

    # calculate the weighted sum
    tmp = 0
    for measure in weights.keys():
        tmp += re_identification_risk[measure] * weights[measure]

    risk = tmp / sum(weights.values())

    return jsonify(risk=risk), 200


@api.route('/datasets/<int:dataset_id>/analyze-risk', methods=['POST'])
@privileged_user_required
def analyze_risk(dataset_id):
    """Analyzes the risk for the given dataset. Privileged user required.
    Identifying attributes (e.g. health insurance number) allow a re-identification of the \
    affected persons and are therefore redacted. Quasi-identifying attributes (e.g. zip code \
    or gender) pose a threat together with external information and are therefore transformed, \
    for example with k-anonymity. Sensitive attributes (e.g. medical diagnoses) should not be \
    disclosed to the public and can be protected with t-closeness or l-diversity. Non-sensitive \
    attributes remain unchanged.
    Access token has to be set in the Authorization header using the Bearer schema.
    ```metadata``` is only needed for differential privacy and must therefore not be specified.
    Example of a valid request:
    ```
    {
      "attributes": [
        {
          "attribute_name": "zipcode",
          "attribute_type": "quasi-identifying",
          "attribute_hierarchy": [
            [
              "81667",
              "8166*",
              "816**",
              "81***",
              "8****",
              "*****"
            ],
            [
              "81668",
              "8166*",
              "816**",
              "81***",
              "8****",
              "*****"
            ]
          ]
        },
        {
          "attribute_name": "age",
          "attribute_type": "quasi-identifying",
          "attribute_hierarchy": [
            [
              "18",
              "young-adult",
              "*"
            ],
            [
              "19",
              "young-adult",
              "*"
            ],
            [
              "20",
              "young-adult",
              "*"
            ]
          ]
        },
        {
          "attribute_name": "disease",
          "attribute_type": "sensitive"
        }
      ],
      "privacy_models": [
        {
          "model_name": "k-anonymity",
          "model_params": {
            "k": 5
          }
        },
        {
          "model_name": "l-diversity_distinct",
          "attribute_name": "disease",
          "model_params": {
            "l": 2
          }
        }
      ],
      "suppression_limit": 0.2
    }
    ```
    ---
    tags:
      - Datasets
    parameters:
      - in: path
        name: dataset_id
        schema:
          type: integer
        required: true
        description: ID of the dataset
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/Config'
    responses:
      200:
        description: Risk analysis has been successful.
        content:
          application/json:
            schema:
              properties:
                attribute_generalization:
                  type: array
                  items:
                    type: string
                risk_profile:
                  type: array
                  items:
                    type: string

      400:
        description: Bad Request
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Bad Request
                msg:
                  type: string
                  enum:
                    - Missing JSON.
      401:
        description: Authorization Header is missing or token has been revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Missing Authorization Header
                    - Token has been revoked
      403:
        description: User is not privileged.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Forbidden
                msg:
                  type: string
                  enum:
                    - Privileged users only!
      404:
        description: No dataset with the ```dataset_id``` could be found.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Not Found
                msg:
                  type: string
                  enum:
                    - Dataset not found.
      500:
        description: Internal Server Error.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Internal Server Error
                msg:
                  type: string
    """
    dataset = Dataset.query.get(dataset_id)

    # raise NotFound error if dataset does not exist
    if dataset is None:
        raise NotFound('Dataset not found.')

    if not request.json:
        raise BadRequest('Missing JSON.')

    # anonymize dataset with given config
    config = config_schema.load(request.json)
    result = anonymize_dataset(config, dataset)

    return jsonify(create_analyzation_response(result)), 200


@api.route('/datasets/<int:dataset_id>/create-generalization-hierarchy', methods=['POST'])
@privileged_user_required
def create_generalization_hierarchy(dataset_id):
    """Creates generalization hierarchy for the given attribute. Privileged user required.
    Access token has to be set in the Authorization header using the Bearer schema.
    Supports redaction- and interval-based generalization, so either ```intervals``` or \
    ```redaction_order``` is allowed.
    Examples of valid requests:
    ```
    {
      "transformation_type": "redaction",
      "attribute_name": "zipcode",
      "redaction_order": "left_to_right"
    }
    ```
    ```
    {
      "attribute_name": "age",
      "transformation_type": "interval",
      "intervals": [
        {
          "label": "young",
          "min": 0,
          "max": 20
        },
        {
          "label": "adult",
          "min": 20,
          "max": 70
        },
        {
          "label": "elderly",
          "min": 70,
          "max": 100
        }
      ]
    }
    ```
    ---
    tags:
      - Datasets
    parameters:
      - in: path
        name: dataset_id
        schema:
          type: integer
        required: true
        description: ID of the dataset
    requestBody:
      required: true
      content:
        application/json:
          schema:
            $ref: '#/components/schemas/HierarchyGeneration'
    responses:
      200:
        description: Generalization hierarchy has been successfully created.
        content:
          application/json:
            schema:
              properties:
                hierarchy:
                  type: array
                  items:
                    type: string
      400:
        description: Bad Request
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Bad Request
                msg:
                  type: string
                  enum:
                    - Missing JSON.
      401:
        description: Authorization Header is missing or token has been revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Missing Authorization Header
                    - Token has been revoked
      403:
        description: User is not privileged.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Forbidden
                msg:
                  type: string
                  enum:
                    - Privileged users only!
      404:
        description: No dataset with the ```dataset_id``` could be found.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Not Found
                msg:
                  type: string
                  enum:
                    - Dataset not found.
      500:
        description: Internal Server Error.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Internal Server Error
                msg:
                  type: string
    """
    dataset = Dataset.query.get(dataset_id)

    # raise NotFound error if dataset does not exist
    if dataset is None:
        raise NotFound('Dataset not found.')

    if not request.json:
        raise BadRequest('Missing JSON.')

    # create desired hierarchy
    hierarchy_request = hierarchy_generation_schema.load(request.json)
    result = create_hierarchy(hierarchy_request, dataset)

    return jsonify(hierarchy=result), 200
