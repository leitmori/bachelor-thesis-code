"""Creates the api blueprint."""

from flask import Blueprint

api = Blueprint('api', __name__)

# import blueprint modules - avoid circular dependencies
from app.api.v1 import errors  # noqa: E402,F401
from app.api.v1.authenticator import authentication  # noqa: E402,F401
from app.api.v1.routes import user_handler, dataset_handler, project_handler  # noqa: E402,F401
