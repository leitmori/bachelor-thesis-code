"""Contains helper functions for authentication such as custom decorators for Flask-JWT-Extended."""
from datetime import datetime
from functools import wraps

from flask import current_app
from flask_jwt_extended import decode_token, get_jwt_claims, verify_jwt_in_request
from sqlalchemy.orm.exc import NoResultFound

from app import jwt_manager
from app.api.v1.errors import Forbidden, InternalServerError
from app.models.models import Token


def add_refresh_token_to_database(encoded_token):
    """Add encoded JWT from user to tokens table.

    Args:
        encoded_token (str): Refresh token.

    """
    decoded_token = decode_token(encoded_token)
    jti = decoded_token['jti']
    user_id = decoded_token[current_app.config['JWT_IDENTITY_CLAIM']]
    expiration_date = datetime.fromtimestamp(decoded_token['exp'])
    token = Token(jti=jti, user_id=user_id, revoked_status=False, expiration_date=expiration_date)
    token.save()


def revoke_token(jti):
    """Revoke the token with given jti in tokens table.

    Args:
        jti (str): JSON Web Token Identifier.

    Raises:
        InternalServerError: If no refresh token was found with given jti.

    """
    try:
        token = Token.query.filter_by(jti=jti).one()
        token.revoked_status = True
        token.save()
    except NoResultFound:
        raise InternalServerError('Error revoking the token')


@jwt_manager.token_in_blacklist_loader
def check_if_token_revoked(decoded_token):
    """Check if a token has been revoked or not. Callback function for Flask-JWT-Extended.

    Args:
        decoded_token (str): Decoded JSON Web Token.

    Returns:
        bool: Indicates whether token was revoked. True -> revoked token, False -> valid token.

    """
    jti = decoded_token['jti']
    try:
        token = Token.query.filter_by(jti=jti).one()
        return token.revoked_status
    except NoResultFound:
        return True


@jwt_manager.user_claims_loader
def add_claims_to_access_token(user):
    """Add custom claims to the access token.

    Allows distinction of privileged and non-privileged users. Will be called whenever
    create_access_token is used.

    Args:
        user (User): Current user.

    Returns:
        dict: Claims to be added to the access token.

    """
    return {'privileged': user.privileged_status}


@jwt_manager.user_identity_loader
def user_identity_lookup(user):
    """Define what the identity of a token should be.

    Will be called whenever create_access_token or create_refresh_token is used.

    Args:
        user (User): Current user.

    Returns:
        int: Identity of the token.

    """
    return user.user_id


def privileged_user_required(fn):
    """Privileged user required decorator.

    Custom decorator that verifies the JWT is present in the request, as well as insuring that
    this user is privileged (privileged_status must be True).

    Args:
        fn (function): Function to be decorated.

    Raises:
        Forbidden: If user is not privileged.

    Returns:
        function: Decorated function.

    """
    @wraps(fn)
    def wrapper(*args, **kwargs):
        verify_jwt_in_request()
        claims = get_jwt_claims()
        if not claims['privileged']:
            raise Forbidden('Privileged users only!')
        else:
            return fn(*args, **kwargs)

    return wrapper
