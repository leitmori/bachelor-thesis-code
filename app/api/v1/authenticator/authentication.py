"""This module contains the authentication routes."""

from flask import jsonify, request
from flask_jwt_extended import (create_access_token, create_refresh_token, get_jwt_identity,
                                get_raw_jwt, jwt_refresh_token_required)

from app.api.v1 import api
from app.api.v1.authenticator.helpers import add_refresh_token_to_database, revoke_token
from app.api.v1.errors import BadRequest, Unauthorized
from app.models.models import User


@api.route('/login', methods=['POST'])
def login():
    """Authenticates the user using the provided email address and password.
    ---
    tags:
      - Authentication
    requestBody:
      required: true
      content:
        application/json:
          schema:
            properties:
              email_address:
                type: string
                format: email
              password:
                type: string
                format: password
            required:
              - email_address
              - password
    responses:
      200:
        description: Token have been successfully created.
        content:
          application/json:
            schema:
              properties:
                access_token:
                  type: string
                  description: Access token (valid for 15min).
                refresh_token:
                  type: string
                  description: Refresh token (valid for 7d).
      400:
        description: Missing JSON.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Bad Request
                msg:
                  type: string
                  enum:
                    - Missing JSON.
      401:
        description: Invalid email address or password.
        content:
          application/json:
            schema:
              properties:
                error:
                  type: string
                  enum:
                    - Unauthorized
                msg:
                  type: string
                  enum:
                    - Invalid username or password.
    """
    if request.json is None:
        raise BadRequest('Missing JSON.')
    email_address = request.json.get('email_address', None)
    password = request.json.get('password', None)
    if email_address is None or password is None:
        raise Unauthorized('Email address or password field empty.')

    # get current user in db and verify password
    user = User.query.filter_by(email_address=email_address).first()
    if user is None or not user.verify_password(password):
        raise Unauthorized('Invalid username or password.')

    # create JWTs and add refresh token to db
    access_token = create_access_token(identity=user)
    refresh_token = create_refresh_token(identity=user)
    add_refresh_token_to_database(refresh_token)

    return jsonify(access_token=access_token, refresh_token=refresh_token), 200


@api.route('/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    """Creates a new access token.
    Refresh token has to be set in the Authorization header using the Bearer schema.
    ---
    tags:
      - Authentication
    responses:
      200:
        description: Access token has been successfully created.
        content:
          application/json:
            schema:
              properties:
                access_token:
                  type: string
                  description: Access token (valid for 15min).
      401:
        description: Authorization Header is missing or token has been revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Missing Authorization Header
                    - Token has been revoked
    """
    # get current user
    user = User.query.filter_by(user_id=get_jwt_identity()).first()

    # create new access token
    access_token = create_access_token(identity=user)

    return jsonify(access_token=access_token), 200


@api.route('/logout', methods=['DELETE'])
@jwt_refresh_token_required
def logout():
    """Invalidates the provided refresh token.
    Refresh token has to be set in the Authorization header using the Bearer schema.
    ---
    tags:
      - Authentication
    responses:
      200:
        description: Refresh token has been successfully revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Successfully logged out.
      401:
        description: Authorization Header is missing or token has been revoked.
        content:
          application/json:
            schema:
              properties:
                msg:
                  type: string
                  enum:
                    - Missing Authorization Header
                    - Token has been revoked
    """
    jti = get_raw_jwt()['jti']
    revoke_token(jti)

    return jsonify({"msg": "Successfully logged out."}), 200
