"""This module defines custom command-line commands."""

import os
import sys

import click


def register(app):
    """Register commands in application instance.

    Args:
        app (flask.Flask): The flask app to initialise with.

    """

    @app.cli.command('test')
    @click.option('--coverage', is_flag=True, help='Run tests under code coverage.')
    def test(coverage):  # pragma: no cover
        """Run the unit tests."""
        if coverage and not os.environ.get('FLASK_COVERAGE'):
            import subprocess
            os.environ['FLASK_COVERAGE'] = '1'
            sys.exit(subprocess.call(sys.argv))

        import unittest
        tests = unittest.TestLoader().discover('tests')
        result = unittest.TextTestRunner(verbosity=2).run(tests)

        from priori import COV
        if COV:
            COV.stop()
            COV.save()
            print('\nCoverage Summary:')
            basedir = os.path.dirname(os.path.realpath('priori.py'))
            COV.report()
            covdir = os.path.join(basedir, 'coverage')
            COV.html_report(directory=covdir)
            print('HTML version: file://%s/index.html' % covdir)
            COV.erase()

        # check if all tests have passed
        sys.exit(not result.wasSuccessful())

    @app.cli.command('add_user')
    @click.argument('first_name')
    @click.argument('last_name')
    @click.argument('institution')
    @click.argument('email_address')
    @click.password_option(help='Password of user.')
    @click.option('--privileged', '-P', is_flag=True,
                  help='If this flag is set, the user is privileged.')
    def add_user(first_name, last_name, institution, email_address, password, privileged):
        """Create a new user.

        Args:
            first_name (str): First name.
            last_name (str): Last name.
            institution (str): Institution for which the user works.
            email_address (str): Email address.
            password (str): Password of user.
            privileged (bool): True -> user has admin rights,
            False -> user is not privileged.

        Raises:
            ValueError: Invalid name or email address.

        """
        from app.models.schemas import UserSchema
        name = f"{first_name} {last_name}"

        # validate input
        try:
            user = UserSchema().load({"name": name, "institution": institution,
                                      "email_address": email_address, "password": password,
                                      "privileged_status": privileged})
            user.save()
            print('User creation successful')

        except Exception as e:
            if hasattr(e, 'message'):
                print(e.message)
            else:
                print(e)

    @app.cli.command('clear_expired_tokens')
    def clear_expired_tokens():
        """Delete all expired tokens in database."""
        from app.models.models import Token
        Token.delete_expired_tokens()
        print('Deletion of expired tokens successful')
