"""Defines all marshmallow schemas for models."""

import re
from enum import Enum

from marshmallow import (EXCLUDE, Schema, ValidationError, fields, post_dump, post_load, pre_load,
                         validate, validates_schema)
from marshmallow_sqlalchemy import auto_field

from app import ma
from app.models.models import Dataset, Project, User


class PrivacyModel(str, Enum):
    """Supported privacy models."""

    K_ANONYMITY = 'k-anonymity'
    L_DIVERSITY_DISTINCT = 'l-diversity_distinct'
    L_DIVERSITY_GRASSBERGER_ENTROPY = 'l-diversity_grassberger_entropy'
    L_DIVERSITY_RECURSIVE = 'l-diversity_recursive'
    L_DIVERSITY_SHANNON_ENTROPY = 'l-diversity_shannon_entropy'
    T_CLOSENESS_EQUAL_DISTANCE = 't-closeness_equal_distance'
    T_CLOSENESS_ORDERED_DISTANCE = 't-closeness_ordered_distance'
    EPSILON_DELTA_DIFFERENTIAL_PRIVACY = 'epsilon-delta-differential_privacy'


class Attribute(str, Enum):
    """Supported attribute types."""

    IDENTIFYING = 'identifying'
    QUASIIDENTIFYING = 'quasi-identifying'
    SENSITIVE = 'sensitive'
    INSENSITIVE = 'insensitive'


class HierarchyType(str, Enum):
    """Supported hierarchy types."""

    REDACTION = 'redaction'
    INTERVAL = 'interval'


class RedactionOrder(str, Enum):
    """Supported redaction sequences."""

    LEFT_TO_RIGHT = 'left_to_right'
    RIGHT_TO_LEFT = 'right_to_left'


class DataType(str, Enum):
    """Supported data types for differential privacy."""

    BOOL = 'bool'
    DATETIME = 'datetime'
    FLOAT = 'float'
    INT = 'int'
    STR = 'str'


class LoadKeySchemaMixin:
    """Processing field "load_key" extra parameter.

    Works for fields like "data_key", but for deserialization only.

    """

    def _replace_keys(self, data, pairs):
        for from_key, to_key in pairs:
            if from_key in data:
                data[to_key] = data.pop(from_key)
        return data

    @pre_load(pass_many=True)
    def __pre_load(self, data, many, **kwargs):
        pairs = tuple(
            (field.metadata['load_key'], field.data_key or field_name)
            for field_name, field in self.fields.items() if 'load_key' in field.metadata
        )
        if many:
            return [self._replace_keys(v, pairs) for v in data]
        else:
            return self._replace_keys(data, pairs)


class BaseModelSchema(ma.SQLAlchemyAutoSchema):
    """This class contains behavior needed in all model schemas."""

    def handle_error(self, exc, data, **kwargs):
        """Raise custom BadRequest error if validation fails."""
        from app.api.v1.errors import BadRequest

        message = exc.messages['_schema'] if '_schema' in exc.messages else exc.messages
        raise BadRequest(message)


class BaseSchema(Schema):
    """This class contains behavior needed in all other schemas."""

    def handle_error(self, exc, data, **kwargs):
        """Raise custom BadRequest error if validation fails."""
        from app.api.v1.errors import BadRequest

        message = exc.messages['_schema'] if '_schema' in exc.messages else exc.messages
        raise BadRequest(message)


class Validator:
    """This class defines methods to validate the marshmallow schemas."""

    @staticmethod
    def user_exists(user_id):
        """Validate that user with given user_id exists.

        Args:
            user_id (int): Given user_id to validate.
        Raises:
            ValidationError: If user with given user_id does not exist.

        """
        if User.query.get(user_id) is None:
            raise ValidationError('User not found.')

    @staticmethod
    def dataset_exists(dataset_id):
        """Validate that dataset with given dataset_id exists.

        Args:
            dataset_id (int): Given dataset_id to validate.
        Raises:
            ValidationError: If dataset with given dataset_id does not exist.

        """
        if Dataset.query.get(dataset_id) is None:
            raise ValidationError('Dataset not found.')


class UserSchema(BaseModelSchema):
    """Defines a marshmallow schema for the user model.

    Dumped schema contains keys: user_id, name, institution, email_address, privileged_status.

    Loading schema needs keys: name, institution, email_address, password, privileged_status.

    Args:
        BaseModelSchema (Schema): Basic schema class.

    """

    user_id = auto_field(dump_only=True)
    password = fields.String(load_only=True, required=True, validate=validate.Length(min=1))
    email_address = fields.Email(required=True)
    privileged_status = auto_field(missing=False)

    class Meta:
        """Sets model of schema and fields to serialize/deserialize."""

        model = User
        load_instance = True
        fields = ('user_id', 'name', 'institution',
                  'email_address', 'password', 'privileged_status')

    @validates_schema
    def validates_schema(self, data, **kwargs):
        """Validate given data before loading.

        Args:
            data (dict of Str : obj): Data to validate.

        Raises:
            ValidationError: If email_address is already in use.

        """
        if not re.match(r"(^[a-zA-Z ,.'-]+$)", data['name']):
            raise ValidationError('Invalid name.', field_name='name')
        if User.query.filter_by(email_address=data['email_address']).first() is not None:
            raise ValidationError('Email address is already in use.', field_name='email_address')


class DatasetSchema(BaseModelSchema):
    """Defines a marshmallow schema for the dataset model.

    Dumped schema contains keys: dataset_id, name, description, creator, created_at.

    Loading schema needs keys: name, description.

    Args:
        BaseModelSchema (Schema): Basic schema class.

    """

    dataset_id = auto_field(dump_only=True)
    creator = fields.Nested(UserSchema, dump_only=True)
    created_at = auto_field(dump_only=True)

    class Meta:
        """Sets model of schema and fields to serialize/deserialize."""

        model = Dataset
        load_instance = True
        fields = ('dataset_id', 'name', 'description', 'creator', 'created_at')


class MetadataSchema(BaseSchema):
    """Defines a marshmallow schema for metadata (needed for differential privacy).

    Loading schema needs keys: datatype, min (if datatype is float or int),
    max (if datatype is float or int), cardinality (if datatype is str).

    Args:
        BaseSchema (Schema): Basic schema class.

    """

    datatype = fields.Str(validate=validate.OneOf(list(DataType)),
                          required=True)
    min = fields.Float(missing=None)
    max = fields.Float(missing=None)
    cardinality = fields.Int(missing=None)


class AttributeSchema(BaseSchema):
    """Defines a marshmallow schema for an attribute in a dataset.

    Loading schema needs keys: model_name, attribute_name (if model_name != k-anonymity),
    model_params.

    Args:
        BaseSchema (Schema): Basic schema class.

    """

    attribute_name = fields.Str(required=True)
    attribute_type = fields.Str(validate=validate.OneOf(list(Attribute)),
                                required=True, load_only=True)
    attribute_hierarchy = fields.List(fields.List(fields.String), allow_none=True, load_only=True)
    metadata = fields.Nested(MetadataSchema)

    @validates_schema
    def validates_schema(self, data, **kwargs):
        """Validate given data before loading.

        Args:
            data (dict of Str : obj): Data to validate.

        Raises:
            ValidationError: If attribute is invalid.

        """
        if (data['attribute_type'] == Attribute.IDENTIFYING or data['attribute_type'] ==
                Attribute.INSENSITIVE) and 'attribute_hierarchy' in data:
            raise ValidationError('This attribute does not require a hierarchy.',
                                  field_name='attribute_hierarchy')


class PrivacyModelSchema(BaseSchema):
    """Defines a marshmallow schema for the privacy model.

    Loading schema needs keys: model_name, attribute_name (if model_name != k-anonymity),
    model_params.

    Args:
        BaseSchema (Schema): Basic schema class.

    """

    model_name = fields.Str(validate=validate.OneOf(list(PrivacyModel)),
                            required=True)
    attribute_name = fields.Str(required=False)
    model_params = fields.Dict(keys=fields.Str(), values=fields.Float(), required=True)

    @post_load
    def cast_to_int(self, in_data, **kwargs):
        """Cast non-interactive privacy model params to int.

        Args:
            in_data (dict): Input dict.

        Returns:
            dict: Output dict.

        """
        if in_data['model_name'] != PrivacyModel.EPSILON_DELTA_DIFFERENTIAL_PRIVACY:
            in_data['model_params'] = dict([a, int(x)] for a, x in in_data['model_params'].items())
        return in_data

    @validates_schema
    def validates_schema(self, data, **kwargs):
        """Validate given data before loading.

        Args:
            data (dict of Str : obj): Data to validate.

        Raises:
            ValidationError: If privacy model is invalid.

        """
        if data['model_name'] != PrivacyModel.EPSILON_DELTA_DIFFERENTIAL_PRIVACY:
            if not all(param.is_integer()for param in list(data['model_params'].values())):
                raise ValidationError('Invalid type for model_params: float')

        if data['model_name'] == PrivacyModel.K_ANONYMITY:
            if 'attribute_name' in data:
                raise ValidationError('attribute_name not required for k-anonymity.',
                                      field_name='attribute_name')
            if len(data['model_params']) != 1 or 'k' not in data['model_params']:
                raise ValidationError('Invalid model_params for k-anonymity.',
                                      field_name='model_params')

        elif data['model_name'] == PrivacyModel.L_DIVERSITY_DISTINCT:
            if 'attribute_name' not in data:
                raise ValidationError('attribute_name required for l-diversity_distinct.',
                                      field_name='attribute_name')
            if len(data['model_params']) != 1 or 'l' not in data['model_params']:
                raise ValidationError('Invalid model_params for l-diversity_distinct.',
                                      field_name='model_params')

        elif data['model_name'] == PrivacyModel.L_DIVERSITY_GRASSBERGER_ENTROPY:
            if 'attribute_name' not in data:
                raise ValidationError(
                    'attribute_name required for l-diversity_grassberger_entropy.',
                    field_name='attribute_name')
            if len(data['model_params']) != 1 or 'l' not in data['model_params']:
                raise ValidationError('Invalid model_params for l-diversity_grassberger_entropy.',
                                      field_name='model_params')

        elif data['model_name'] == PrivacyModel.L_DIVERSITY_RECURSIVE:
            if 'attribute_name' not in data:
                raise ValidationError('attribute_name required for l-diversity_recursive.',
                                      field_name='attribute_name')
            if len(data['model_params']) != 2 or not {'l', 'c'} <= data['model_params'].keys():
                raise ValidationError('Invalid model_params for l-diversity_recursive.',
                                      field_name='model_params')

        elif data['model_name'] == PrivacyModel.L_DIVERSITY_SHANNON_ENTROPY:
            if 'attribute_name' not in data:
                raise ValidationError('attribute_name required for l-diversity_shannon_entropy.',
                                      field_name='attribute_name')
            if len(data['model_params']) != 1 or 'l' not in data['model_params']:
                raise ValidationError('Invalid model_params for l-diversity_shannon_entropy.',
                                      field_name='model_params')

        elif data['model_name'] == PrivacyModel.T_CLOSENESS_EQUAL_DISTANCE:
            if 'attribute_name' not in data:
                raise ValidationError('attribute_name required for t-closeness_equal_distance.',
                                      field_name='attribute_name')
            if len(data['model_params']) != 1 or 't' not in data['model_params']:
                raise ValidationError('Invalid model_params for t-closeness_equal_distance.',
                                      field_name='model_params')

        elif data['model_name'] == PrivacyModel.T_CLOSENESS_ORDERED_DISTANCE:
            if 'attribute_name' not in data:
                raise ValidationError('attribute_name required for t-closeness_ordered_distance.',
                                      field_name='attribute_name')
            if len(data['model_params']) != 1 or 't' not in data['model_params']:
                raise ValidationError('Invalid model_params for t-closeness_ordered_distance.',
                                      field_name='model_params')

        elif data['model_name'] == PrivacyModel.EPSILON_DELTA_DIFFERENTIAL_PRIVACY:
            if 'attribute_name' in data:
                raise ValidationError(
                    'attribute_name not required for epsilon-delta-differential_privacy.',
                    field_name='attribute_name')
            if (len(data['model_params']) != 2 or not
                    {'epsilon', 'delta'} <= data['model_params'].keys()):
                raise ValidationError(
                    'Invalid model_params for epsilon-delta-differential_privacy.',
                    field_name='model_params')


class ConfigSchema(BaseSchema):
    """Defines a marshmallow schema for the configuration of an analysis or anonymization.

    Loading schema needs keys: attributes, privacy_models and suppression_limit.

    Args:
        BaseSchema (Schema): Basic schema class.

    """

    attributes = fields.Nested(AttributeSchema, required=True, many=True)
    privacy_models = fields.Nested(PrivacyModelSchema, required=True, many=True)
    suppression_limit = fields.Float(required=False)

    @validates_schema
    def validates_schema(self, data, **kwargs):
        """Validate given data before loading.

        Args:
            data (dict of Str : obj): Data to validate.

        Raises:
            ValidationError: If config is invalid.

        """
        privacy_models = data['privacy_models']
        if any('differential_privacy' in value for p_m in privacy_models for value in p_m.values()):
            if 'suppression_limit' in data:
                raise ValidationError('Omit this field for differential privacy.',
                                      field_name='suppression_limit')
            elif len(privacy_models) > 1:
                raise ValidationError(
                    'differential privacy cannot be used together with other privacy models',
                    field_name='privacy_models')


class ProjectSchema(BaseModelSchema):
    """Defines a marshmallow schema for the project model.

    Loading schema needs keys: dataset_id, name, descrption, researcher_id, config.

    Dumped schema contains keys: project_id, name, description, dataset_id, creator, researcher,
    attributes, created_at.

    Args:
        BaseModelSchema (Schema): Basic schema class.

    """

    project_id = auto_field(dump_only=True)
    dataset_id = auto_field(validate=Validator.dataset_exists)
    creator = fields.Nested(UserSchema, dump_only=True)
    researcher_id = auto_field(load_only=True, validate=Validator.user_exists)
    researcher = fields.Nested(UserSchema, dump_only=True)
    attributes = fields.Nested(AttributeSchema, dump_only=True)
    config = fields.Nested(ConfigSchema, required=True, load_only=True)
    created_at = auto_field(dump_only=True)

    class Meta:
        """Sets model of schema and fields to serialize/deserialize."""

        model = Project
        load_instance = True
        fields = ('project_id', 'name', 'description', 'dataset_id', 'creator',
                  'researcher', 'researcher_id', 'attributes', 'config', 'created_at')

    @post_dump(pass_original=True)
    def add_attributes(self, data, original_data, **kwargs):
        """Add attributes to dict.

        Args:
            data (dict): Project as dict (input).
            original_data (Project): Project as database model.

        Returns:
            dict: Project as dict (output).

        """
        from app.api.v1.routes.helper import clean_empty
        attributes = clean_empty(original_data.config['attributes'])
        result = []
        for attribute in attributes:
            tmp = {}
            tmp['attribute_name'] = attribute['attribute_name']
            if original_data.uses_dp():
                if attribute['attribute_type'] == Attribute.IDENTIFYING:
                    continue
                tmp['metadata'] = attribute['metadata']
            result.append(tmp)
        data['attributes'] = result
        return data


class IntervalHierarchySchema(BaseSchema):
    """Defines a marshmallow schema for an interval hierarchy.

    Loading schema needs keys: min, max, label.

    Args:
        BaseSchema (Schema): Basic schema class.

    """

    min = fields.Int(required=True)
    max = fields.Int(required=True)
    label = fields.Str(required=True)

    @validates_schema
    def validates_schema(self, data, **kwargs):
        """Validate given data before loading.

        Args:
            data (dict of Str : obj): Data to validate.

        Raises:
            ValidationError: If interval hierarchy is invalid.

        """
        if data['max'] <= data['min']:
            raise ValidationError('max has to be greater than min.', field_name='max')


class HierarchyGenerationSchema(BaseSchema):
    """Defines a marshmallow schema for the hierarchy generation.

    Loading schema needs keys: transformation_type, attribute_name, redaction_order, intervals.

    Args:
        BaseSchema (Schema): Basic schema class.

    """

    transformation_type = fields.Str(validate=validate.OneOf(list(HierarchyType)), required=True)
    attribute_name = fields.Str(required=True)
    redaction_order = fields.Str(validate=validate.OneOf(list(RedactionOrder)))
    intervals = fields.Nested(IntervalHierarchySchema, many=True)

    @validates_schema
    def validates_schema(self, data, **kwargs):
        """Validate given data before loading.

        Args:
            data (dict of Str : obj): Data to validate.

        Raises:
            ValidationError: If params for hierarchy generation are invalid.

        """
        if data['transformation_type'] == HierarchyType.REDACTION and 'redaction_order' not in data:
            raise ValidationError('Missing data for required field.', field_name='redaction_order')

        elif data['transformation_type'] == HierarchyType.INTERVAL and 'intervals' not in data:
            raise ValidationError('Missing data for required field.', field_name='intervals')


class AttributeGeneralizationSchema(BaseSchema, LoadKeySchemaMixin):
    """Mapper between camelCase and snake_case.

    Args:
        LoadKeySchemaMixin (Schema): Adds support for load_key parameter.

    """

    generalization_level = fields.Int(load_key='generalizationLevel')
    attribute_name = fields.Str(load_key='name')

    class Meta:
        """Fields to serialize/deserialize."""

        unknown = EXCLUDE


class RiskIntervalSchema(BaseSchema, LoadKeySchemaMixin):
    """Mapper between camelCase and snake_case.

    Args:
        LoadKeySchemaMixin (Schema): Adds support for load_key parameter.

    """

    interval = fields.Str()
    records_with_maximal_risk_within_interval = fields.Float(
        load_key='recordsWithMaximalRiskWithinInterval')
    records_with_risk_within_interval = fields.Float(
        load_key='recordsWithRiskWithinInterval')
