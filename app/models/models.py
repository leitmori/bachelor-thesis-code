"""This module defines all models."""

from datetime import datetime

from werkzeug.security import check_password_hash, generate_password_hash

from app import db


class BaseMixin(object):
    """This class contains behavior that is needed in almost all models."""

    def save(self):
        """Save object in database. For this purpose, the object is added to the db.session."""
        db.session.add(self)
        db.session.commit()

    def delete(self):
        """Remove object in database."""
        db.session.delete(self)
        db.session.commit()


class User(db.Model, BaseMixin):
    """This class represents the user model.

    Args:
        db (SQLAlchemy): SQLAlchemy object.
        BaseMixin (ModelMixin): BaseMixin contains the behavior that is needed by all models.

    Attributes:
        user_id (int): Unique ID of the user.
        name (str): Name of user.
        institution (str): Institution for which the user works.
        email_address (str): Email address of user, also serves as username.
        password_hash (str): Salted-hash-password of user.
        privileged_status (bool): True -> user has admin rights, False -> user is not privileged.

    """

    __tablename__ = 'users'

    # columns
    user_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    institution = db.Column(db.String(100), nullable=False)
    email_address = db.Column(db.String(255), nullable=False, unique=True)
    password_hash = db.Column(db.String(128), nullable=False)
    privileged_status = db.Column(db.Boolean, nullable=False, default=False)

    @property
    def password(self):
        """Raise error when trying to access password.

        Raises:
            AttributeError: Password is not a readable attribute.

        """
        raise AttributeError('password is not a readable attribute')

    @password.setter
    def password(self, password):
        """Set password."""
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        """Verify that a given password string matches the salted-hash-password.

        Args:
            password (str): Password to check.

        Returns:
            bool: True -> correct password, False -> wrong password.

        """
        return check_password_hash(self.password_hash, password)


class Dataset(db.Model, BaseMixin):
    """This class represents the dataset model.

    Args:
        db (SQLAlchemy): SQLAlchemy object.
        BaseMixin (ModelMixin): BaseMixin contains the behavior that is needed by all models.

    Attributes:
        dataset_id (int): Unique ID of the dataset.
        name (str): Name of the dataset.
        description (str): String describing the dataset.
        creator_id (int): User ID of creator.
        filename (str): Filename of associated CSV in 'uploads'.
        created_at (datetime): Timestamp of creation.
        creator (User): User object of creator.

    """

    __tablename__ = 'datasets'

    # columns
    dataset_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(5000), nullable=True)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.user_id'), nullable=False)
    filename = db.Column(db.String(100), nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    # relationships
    creator = db.relationship('User', lazy='joined')


class Project(db.Model, BaseMixin):
    """This class represents the project model.

    Args:
        db (SQLAlchemy): SQLAlchemy object.
        BaseMixin (ModelMixin): BaseMixin contains the behavior that is needed by all models.

    Attributes:
        project_id (int): Unique ID of the project.
        name (str): Name of the project.
        description (str): String describing the project.
        dataset_id (int): Unique ID of the dataset.
        creator_id (int): User ID of creator.
        researcher_id (int): User ID of researcher.
        filename (str): Filename of associated CSV in 'projects'.
        config (JSON): Project config (privacy models, params etc.) as JSON.
        created_at (datetime): Timestamp of creation.
        dataset (Dataset): Associated dataset as object.
        creator (User): User object of creator.
        researcher (User): User object of researcher.

    """

    __tablename__ = 'projects'

    # columns
    project_id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), nullable=False)
    description = db.Column(db.String(5000), nullable=True)
    dataset_id = db.Column(db.Integer, db.ForeignKey('datasets.dataset_id'), nullable=False)
    creator_id = db.Column(db.Integer, db.ForeignKey('users.user_id'), nullable=False)
    researcher_id = db.Column(db.Integer, db.ForeignKey('users.user_id'), nullable=False)
    filename = db.Column(db.String(100), nullable=True)
    config = db.Column(db.JSON, nullable=False)
    created_at = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)

    # relationships
    dataset = db.relationship('Dataset', lazy='joined')
    creator = db.relationship('User', foreign_keys=[creator_id], lazy='joined')
    researcher = db.relationship('User', foreign_keys=[researcher_id], lazy='joined')

    def uses_dp(self):
        """Check if this project uses differential privacy as privacy model.

        Returns:
            bool: True -> Project uses differential privacy, False -> Project does not use
                differential privacy.

        """
        if any('differential_privacy' in value for p_m in self.config['privacy_models']
               for value in p_m.values()):
            return True
        return False


class Token(db.Model, BaseMixin):
    """This class represents the token model.

    Args:
        db (SQLAlchemy): SQLAlchemy object.
        BaseMixin (ModelMixin): BaseMixin contains the behavior that is needed by all models.

    Attributes:
        token_id (int): Unique ID of the token.
        jti (str): JSON Web Token Identifier.
        user_id (int): ID of user for whom the token was issued.
        revoked_status (bool): Indicates whether token was revoked. True -> revoked token,
                False -> valid token.
        expiration_date (datetime): The expiration date of the token.
        user (User): User for whom the token was issued as object.

    """

    __tablename__ = 'tokens'

    # columns
    token_id = db.Column(db.Integer, primary_key=True)
    jti = db.Column(db.String(100), nullable=False, unique=True)
    user_id = db.Column(db.Integer, db.ForeignKey('users.user_id'), nullable=False)
    revoked_status = db.Column(db.Boolean, nullable=False)
    expiration_date = db.Column(db.DateTime, nullable=False)

    # relationships
    user = db.relationship('User', lazy='joined')

    @staticmethod
    def delete_expired_tokens():
        """Delete all expired tokens in database."""
        now = datetime.now()
        expired_tokens = Token.query.filter(Token.expiration_date < now).all()
        for token in expired_tokens:
            db.session.delete(token)
        db.session.commit()
