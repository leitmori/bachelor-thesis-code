"""Configuration module.

Base class contains settings that are common to all configurations,
subclasses define specific settings.
"""
import datetime
import os

from dotenv import load_dotenv

basedir = os.path.abspath(os.path.dirname(__file__))

# load .env file
DOTENV_PATH = os.path.join(basedir, '.env')
if os.path.exists(DOTENV_PATH):
    load_dotenv(DOTENV_PATH)


class Config:
    """Base class."""

    # Secret Key used by Flask and Flask-JWT-Extended
    # How to generate good secret keys: python -c 'import os; print(os.urandom(16))'
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'NEVER_USE_THIS_STRING_IN_PRODUCTION'

    # Disable Flask-SQLAlchemy event notification system
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Enable logging to logs/priori.log instead of STDERR
    LOG_TO_FILE = os.environ.get('LOG_TO_FILE') or False

    # The folder to which files are uploaded to
    UPLOAD_FOLDER = 'uploads'

    # The folder in which project files are stored
    PROJECT_FOLDER = 'projects'

    # Specify the allowed file extensions
    ALLOWED_EXTENSIONS = set(['csv'])

    # The maximum allowed request size
    MAX_CONTENT_LENGTH = 2 * 1024 * 1024 * 1024  # 2 gigabytes

    # URL of the ARXaaS instance
    ARXAAS_URL = os.environ.get('ARXAAS_URL') or 'http://localhost:8080'

    # Specify how the weighted risk should be calculated
    # Structure: 'measure': weight
    WEIGHTED_RISK = {'estimated_journalist_risk': 1, 'estimated_prosecutor_risk': 1,
                     'estimated_marketer_risk': 1}

    # -------JWT-CONFIG-------
    # How long a access token should live before it expires
    JWT_ACCESS_TOKEN_EXPIRES = datetime.timedelta(minutes=15)

    # How long a refresh token should live before it expires
    JWT_REFRESH_TOKEN_EXPIRES = datetime.timedelta(days=7)

    # Enable token revoking
    JWT_BLACKLIST_ENABLED = True

    # Specify token types to check against the blacklist
    JWT_BLACKLIST_TOKEN_CHECKS = 'refresh'

    # -------Flasgger-CONFIG-------
    SWAGGER = {'hide_top_bar': True, 'title': 'PRIORI', 'openapi': '3.0.3', 'specs':
               [{'endpoint': 'apispec', 'route': '/apispec.json'}]}


class DevelopmentConfig(Config):
    """Development settings.

    Args:
        Config (Config): Base Configuration.

    """

    # Use SQLite database if DEV_DATABASE_URL is not provided
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'DEV_DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'data-dev.sqlite')


class TestingConfig(Config):
    """Testing settings.

    Args:
        Config (Config): Base Configuration.

    """

    TESTING = True

    # SQLite database is stored in memory if TEST_DATABASE_URL is not provided
    SQLALCHEMY_DATABASE_URI = os.environ.get('TEST_DATABASE_URL') or 'sqlite://'


class ProductionConfig(Config):
    """Production settings.

    Args:
        Config (Config): Base Configuration.

    """

    # Use SQLite database if DATABASE_URL is not provided
    SQLALCHEMY_DATABASE_URI = os.environ.get(
        'DATABASE_URL') or 'sqlite:///' + os.path.join(basedir, 'data.sqlite')


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'default': ProductionConfig
}
