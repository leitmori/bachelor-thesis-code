# PRIORI – PRIvacy cOmpliant Research Interface

## Quickstart

Download the source code from GitLab and run the docker containers:

```console
$ git clone https://gitlab.com/leitmori/bachelor-thesis-code.git
$ cd bachelor-thesis-code/
$ docker build -t priori:latest .
$ docker run --name priori -d -p 5000:5000 \
-e SECRET_KEY=generate_a_secret_key \
-e PRIORI_CONFIG=development \
-e NAME="John Doe" \
-e INSTITUTION=Institution \
-e MAIL=user@example.com \
-e PASSWORD=password priori:latest
$ docker pull navikt/arxaas
$ docker run --network container:priori navikt/arxaas
```

## Setting up PRIORI

Download the source code from GitLab and create a new virtual environment:

```console
$ git clone https://gitlab.com/leitmori/bachelor-thesis-code.git
$ cd bachelor-thesis-code/
$ python3 -m venv venv
$ source venv/bin/activate
$ pip3 install -r requirements.txt
$ export FLASK_APP="priori.py"
```

Create a .env file and set environment variables as needed ([see Configuration](#configuration)):

```console
$ nano .env
```

Sample .env file for development environment:

```python
# Use the development config
FLASK_ENV=development
PRIORI_CONFIG=development

# Secret Key used by Flask and Flask-JWT-Extended
# How to generate good secret keys: python -c 'import uuid; print(uuid.uuid4().hex)'
SECRET_KEY=generate_a_secret_key
```

Initialize the database and add a privileged user:

```console
$ flask db init
$ flask db migrate
$ flask db upgrade
$ flask add_user John Doe Institution user@example.com -P
```

A privileged user can upload new datasets, add users and create new projects.

#### Run the Flask application

Run PRIORI:

```console
$ flask run
```

Open a new terminal window and run ARXaaS:

```console
$ docker pull navikt/arxaas
$ docker run -p 8080:8080 navikt/arxaas
```

## Documentation

Visit the visual documentation (OpenAPI Specification) in your browser: `localhost:5000/apidocs/`
![OpenAPI](docs/OpenAPI.png)

## Deployment

**Warning:**
You should not use the built-in development server `flask run` for hosting PRIORI. It is strongly recommend to use Gunicorn behind a proxy server (e.g. Nginx) in production.

## Configuration

#### Environment variables

| Variable            | Description                                                                                                                  |
| ------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| `FLASK_ENV`         | Indicates to Flask and extensions what context (`development` or `production`) Flask is running in. Defaults to `production` |
| `PRIORI_CONFIG`     | The config object to use (`development`, `testing` or `production`), defaults to `production`                                |
| `SECRET_KEY`        | **Required**: Secret Key used by Flask and Flask-JWT-Extended, defaults to `NEVER_USE_THIS_STRING_IN_PRODUCTION`             |
| `LOG_TO_FILE`       | Enable logging to logs/priori.log instead of STDERR, defaults to `False`                                                     |
| `ARXAAS_URL`        | URL of the ARXaaS instance, defaults to `http://localhost:8080`                                                              |
| `DATABASE_URL`      | The production database URI, defaults to `sqlite:///data.sqlite`                                                             |
| `DEV_DATABASE_URL`  | The development database URI, defaults to `sqlite:///data-dev.sqlite`                                                        |
| `TEST_DATABASE_URL` | The unittest database URI, defaults to `sqlite://` (in-memory)                                                               |

#### Additional variables in `config.py` module

| Variable                    | Description                                                                                          |
| --------------------------- | ---------------------------------------------------------------------------------------------------- |
| `UPLOAD_FOLDER`             | The folder to which files are uploaded to, defaults to `uploads`                                     |
| `PROJECT_FOLDER`            | The folder in which project files are stored, defaults to `projects`                                 |
| `MAX_CONTENT_LENGTH`        | The maximum allowed request size, defaults to `2 * 1024 * 1024 * 1024` (2 GB)                        |
| `JWT_ACCESS_TOKEN_EXPIRES`  | How long an access token should live before it expires, defaults to `datetime.timedelta(minutes=15)` |
| `JWT_REFRESH_TOKEN_EXPIRES` | How long a refresh token should live before it expires, defaults to `datetime.timedelta(days=7)`     |
| `WEIGHTED_RISK` | How the weighted risk should be calculated. The following measures are available: `estimated_journalist_risk`, `highest_journalist_risk`, `records_affected_by_highest_journalist_risk`, `estimated_prosecutor_risk`, `average_prosecutor_risk`, `highest_prosecutor_risk`,`records_affected_by_highest_prosecutor_risk`, `estimated_marketer_risk`, `lowest_risk`, `records_affected_by_lowest_risk`, `sample_uniques`, `population_uniques` (further information can be found in the [documentation](https://arx.deidentifier.org/anonymization-tool/risk-analysis/) of ARX) |
