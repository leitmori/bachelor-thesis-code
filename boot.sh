#!/bin/bash
# this script is used to boot the docker container
source venv/bin/activate
while true; do
    flask db init
    if [[ "$?" == "1" ]]; then
        break
    fi
    flask db migrate
    flask db upgrade
    flask add_user $NAME $INSTITUTION $MAIL -P --password $PASSWORD
    sleep 5
done

flask run --host=0.0.0.0
