"""Tests the basic functionality."""

import unittest

from flask import current_app

from app import create_app, db


class BasicsTestCase(unittest.TestCase):
    """This class represents the basics test case."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    def test_app_exists(self):
        """Check if app instance exists."""
        self.assertFalse(current_app is None)

    def test_app_is_testing(self):
        """Check if TESTING-Config is selected."""
        self.assertTrue(current_app.config['TESTING'])
