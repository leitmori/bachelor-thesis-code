"""Tests the performance of PRIORI."""
import os
import time
import unittest
from shutil import copyfile

import pandas as pd
from flask import current_app
from helper import (auth_header, create_fake_rows_interactive, create_fake_rows_non_interactive,
                    login_user)

from app import create_app, db
from app.models.models import Dataset, Project, User


def test_performance_non_interactive(client, num_runs, num_rows_values):
    """Test the performance of the non-interactive scenario."""
    # create a privileged user
    password = '123456'
    user = User(name='John Doe', institution='IOSB', email_address='john@doe.com',
                password=password, privileged_status=True)
    user.save()
    results = []

    for num_rows in num_rows_values:
        for i in range(0, num_runs):
            # create dataset with fake data
            fake_data = pd.DataFrame(create_fake_rows_non_interactive(num_rows))
            fake_data.to_csv(os.path.join(current_app.config['UPLOAD_FOLDER'], 'test.csv'),
                             index=False)
            dataset = Dataset(name='Data', description='Demo', creator_id=user.user_id,
                              filename='test.csv')
            dataset.save()

            jwt = login_user(client, user.email_address, password).get_json()['access_token']
            # create hierarchies
            age_hierarchy = client.post(
                '/api/v1/datasets/' + str(dataset.dataset_id) +
                '/create-generalization-hierarchy',
                json={'transformation_type': 'interval', 'attribute_name': 'age',
                      'intervals': [{'label': 'young', 'min': 0, 'max': 20},
                                    {'label': 'adult', 'min': 20, 'max': 70},
                                    {'label': 'elderly', 'min': 70, 'max': 100}]},
                headers=auth_header(jwt)).get_json()['hierarchy']

            zip_code_hierarchy = client.post(
                '/api/v1/datasets/' + str(dataset.dataset_id) +
                '/create-generalization-hierarchy',
                json={'transformation_type': 'redaction', 'attribute_name': 'zip_code',
                      'redaction_order': 'right_to_left'},
                headers=auth_header(jwt)).get_json()['hierarchy']

            projects_data = {
                'name': 'Test',
                'description': 'Test',
                'researcher_id': user.user_id,
                'dataset_id': dataset.dataset_id,
                'config': {
                    'attributes': [
                        {
                            'attribute_name': 'ssn',
                            'attribute_type': 'identifying',
                        },
                        {
                            'attribute_name': 'zip_code',
                            'attribute_type': 'quasi-identifying',
                            'attribute_hierarchy': zip_code_hierarchy
                        },
                        {
                            'attribute_name': 'age',
                            'attribute_type': 'quasi-identifying',
                            'attribute_hierarchy': age_hierarchy
                        },
                        {
                            'attribute_name': 'gender',
                            'attribute_type': 'quasi-identifying',
                            'attribute_hierarchy': [['0', '*'], ['1', '*'],
                                                    ['2', '*'], ['9', '*']]
                        },
                        {
                            'attribute_name': 'blood_type',
                            'attribute_type': 'sensitive'
                        }
                    ],
                    'privacy_models': [
                        {
                            'model_name': 'k-anonymity',
                            'model_params': {
                                'k': 10
                            }
                        },
                        {
                            'model_name': 'l-diversity_distinct',
                            'attribute_name': 'blood_type',
                            'model_params': {
                                'l': 3
                            }
                        }
                    ],
                    'suppression_limit': 0.02
                }
            }

            # measure elapsed time
            start = time.time()
            response = client.post('/api/v1/projects', json=projects_data,
                                   headers=auth_header(jwt))
            end = time.time()
            assert response.status_code == 201
            results.append({'rows': num_rows, 'time': end - start})

    return results


def test_performance_interactive(client, num_runs, num_rows_values):
    """Test the performance of the interactive scenario."""
    # create a privileged user
    password = '123456'
    user = User(name='John Doe', institution='IOSB', email_address='john@doe.com',
                password=password, privileged_status=True)
    user.save()
    results = []

    for num_rows in num_rows_values:
        for i in range(0, num_runs):
            # create project with fake data
            dataset = Dataset(name='Data', description='Demo', creator_id=user.user_id,
                              filename='abc')
            dataset.save()

            fake_data = pd.DataFrame(create_fake_rows_interactive(num_rows))
            fake_data.insert(0, 'ID', range(0, len(fake_data)))
            fake_data.to_csv(os.path.join(current_app.config['PROJECT_FOLDER'], 'test.csv'),
                             index=False)
            config = {
                'attributes': [
                    {
                        'attribute_name': 'zip_code',
                        'attribute_type': 'quasi-identifying',
                        'metadata': {
                            'datatype': 'int',
                            'min': None,
                            'max': None
                        }
                    },
                    {
                        'attribute_name': 'age',
                        'attribute_type': 'quasi-identifying',
                        'metadata': {
                            'datatype': 'int',
                            'min': 0,
                            'max': 100
                        }
                    },
                    {
                        'attribute_name': 'gender',
                        'attribute_type': 'quasi-identifying',
                        'metadata': {
                            'datatype': 'str',
                            'cardinality': 4
                        }

                    },
                    {
                        'attribute_name': 'blood_type',
                        'attribute_type': 'sensitive',
                        'metadata': {
                            'datatype': 'str',
                            'cardinality': 8
                        }
                    }
                ],
                'privacy_models': [
                    {
                        'model_name': 'epsilon-delta-differential_privacy',
                        'model_params': {
                            'epsilon': 3,
                            'delta': 10E-12
                        }
                    }
                ]
            }
            project = Project(name='test', description='demo', dataset_id=dataset.dataset_id,
                              creator_id=user.user_id, researcher_id=user.user_id,
                              filename='test.csv', config=config)
            project.save()

            jwt = login_user(client, user.email_address, password).get_json()['access_token']
            query = {'query': 'SELECT blood_type, AVG(age) FROM PRIORI GROUP BY blood_type'}
            # measure elapsed time
            start = time.time()
            response = client.post('/api/v1/projects/' + str(project.project_id) + '/analyze',
                                   json=query, headers=auth_header(jwt))
            end = time.time()
            assert response.status_code == 200
            results.append({'rows': num_rows, 'time': end - start})

    return results


class PerformanceTest(unittest.TestCase):
    """This class contains the performance tests."""

    def setUp(self):
        """Define test variables and initialize app."""
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        db.create_all()
        self.client = self.app.test_client()

    def tearDown(self):
        """Clean up."""
        db.session.remove()
        db.drop_all()
        self.app_context.pop()

    @unittest.skip('')
    def test_performance_non_interactive_log_scale(self):
        """Run log scale performance test."""
        num_rows_values = [int(10 ** (x/10)) for x in range(20, 61, 5)]
        results = test_performance_non_interactive(self.client, 5, num_rows_values)

        # write result to csv
        results_df = pd.DataFrame(results).groupby('rows').mean().reset_index()
        file_dir = os.path.dirname(os.path.abspath(__file__))
        results_df.to_csv(os.path.join(file_dir, 'log_scale_non_interactive.csv'), index=False)

    @unittest.skip('')
    def test_performance_non_interactive_linear_scale(self):
        """Run linear scale performance test."""
        num_rows_values = [x for x in range(25000, 250001, 25000)]
        results = test_performance_non_interactive(self.client, 10, num_rows_values)

        results_df = pd.DataFrame(results)
        file_dir = os.path.dirname(os.path.abspath(__file__))
        results_df.to_csv(os.path.join(file_dir, 'linear_scale_non_interactive.csv'), index=False)

    @unittest.skip('')
    def test_performance_interactive_log_scale(self):
        """Run log scale performance test."""
        num_rows_values = [int(10 ** (x/10)) for x in range(20, 61, 5)]
        results = test_performance_interactive(self.client, 5, num_rows_values)

        # write result to csv
        results_df = pd.DataFrame(results).groupby('rows').mean().reset_index()
        file_dir = os.path.dirname(os.path.abspath(__file__))
        results_df.to_csv(os.path.join(file_dir, 'log_scale_interactive.csv'), index=False)

    @unittest.skip('')
    def test_performance_interactive_linear_scale(self):
        """Run linear scale performance test."""
        num_rows_values = [x for x in range(25000, 250001, 25000)]
        results = test_performance_interactive(self.client, 10, num_rows_values)

        results_df = pd.DataFrame(results)
        file_dir = os.path.dirname(os.path.abspath(__file__))
        results_df.to_csv(os.path.join(file_dir, 'linear_scale_interactive.csv'), index=False)

    @unittest.skip('')
    def test_k_anonymity_params(self):
        """Test the performance for different values of the k parameter."""
        # create a privileged user
        password = '123456'
        user = User(name='John Doe', institution='IOSB', email_address='john@doe.com',
                    password=password, privileged_status=True)
        user.save()

        file_dir = os.path.dirname(os.path.abspath(__file__))
        dest = os.path.join(file_dir, '..', current_app.config['UPLOAD_FOLDER'], 'test.csv')
        copyfile(os.path.join(file_dir, 'non_interactive.csv'), dest)

        dataset = Dataset(name='Data', description='Demo', creator_id=user.user_id,
                          filename='test.csv')
        dataset.save()
        jwt = login_user(self.client, user.email_address,
                         password).get_json()['access_token']
        # create hierarchies
        age_hierarchy = self.client.post(
            '/api/v1/datasets/' + str(dataset.dataset_id) +
            '/create-generalization-hierarchy',
            json={'transformation_type': 'interval', 'attribute_name': 'age',
                  'intervals': [{'label': 'young', 'min': 0, 'max': 20},
                                {'label': 'adult', 'min': 20, 'max': 70},
                                {'label': 'elderly', 'min': 70, 'max': 100}]},
            headers=auth_header(jwt)).get_json()['hierarchy']

        zip_code_hierarchy = self.client.post(
            '/api/v1/datasets/' + str(dataset.dataset_id) +
            '/create-generalization-hierarchy',
            json={'transformation_type': 'redaction', 'attribute_name': 'zip_code',
                  'redaction_order': 'right_to_left'},
            headers=auth_header(jwt)).get_json()['hierarchy']

        results = []

        for k in range(2, 101, 7):
            for i in range(0, 10):
                projects_data = {
                    'name': 'Test',
                    'description': 'Test',
                    'researcher_id': user.user_id,
                    'dataset_id': dataset.dataset_id,
                    'config': {
                        'attributes': [
                            {
                                'attribute_name': 'ssn',
                                'attribute_type': 'identifying',
                            },
                            {
                                'attribute_name': 'zip_code',
                                'attribute_type': 'quasi-identifying',
                                'attribute_hierarchy': zip_code_hierarchy
                            },
                            {
                                'attribute_name': 'age',
                                'attribute_type': 'quasi-identifying',
                                'attribute_hierarchy': age_hierarchy
                            },
                            {
                                'attribute_name': 'gender',
                                'attribute_type': 'quasi-identifying',
                                'attribute_hierarchy': [['0', '*'], ['1', '*'],
                                                        ['2', '*'], ['9', '*']]
                            },
                            {
                                'attribute_name': 'blood_type',
                                'attribute_type': 'sensitive'
                            }
                        ],
                        'privacy_models': [
                            {
                                'model_name': 'k-anonymity',
                                'model_params': {
                                    'k': k
                                }
                            },
                            {
                                'model_name': 'l-diversity_distinct',
                                'attribute_name': 'blood_type',
                                'model_params': {
                                    'l': 2
                                }
                            }
                        ],
                        'suppression_limit': 0.02
                    }
                }
                # measure elapsed time
                start = time.time()
                response = self.client.post('/api/v1/projects', json=projects_data,
                                            headers=auth_header(jwt))
                end = time.time()
                assert response.status_code == 201
                results.append({'k': k, 'time': end - start})

        results_df = pd.DataFrame(results)
        file_dir = os.path.dirname(os.path.abspath(__file__))
        results_df.to_csv(os.path.join(file_dir, 'k_anonymity.csv'), index=False)

    @unittest.skip('')
    def test_differential_privacy(self):
        """Test different values of the parameter epsilon."""
        # create a privileged user
        password = '123456'
        user = User(name='John Doe', institution='IOSB', email_address='john@doe.com',
                    password=password, privileged_status=True)
        user.save()
        # create project with fake data
        dataset = Dataset(name='Data', description='Demo', creator_id=user.user_id,
                          filename='abc')
        dataset.save()

        file_dir = os.path.dirname(os.path.abspath(__file__))
        dest = os.path.join(file_dir, '..', current_app.config['PROJECT_FOLDER'], 'test.csv')
        copyfile(os.path.join(file_dir, 'interactive.csv'), dest)
        results = []

        for eps in [10 ** (x/100) for x in range(-200, 101, 1)]:
            for i in range(0, 5):
                config = {
                    'attributes': [
                        {
                            'attribute_name': 'zip_code',
                            'attribute_type': 'quasi-identifying',
                            'metadata': {
                                'datatype': 'int'
                            }
                        },
                        {
                            'attribute_name': 'age',
                            'attribute_type': 'quasi-identifying',
                            'metadata': {
                                'datatype': 'int',
                                'min': 0,
                                'max': 100
                            }
                        },
                        {
                            'attribute_name': 'gender',
                            'attribute_type': 'quasi-identifying',
                            'metadata': {
                                'datatype': 'str',
                                'cardinality': 4
                            }

                        },
                        {
                            'attribute_name': 'blood_type',
                            'attribute_type': 'sensitive',
                            'metadata': {
                                'datatype': 'str',
                                'cardinality': 8
                            }
                        }
                    ],
                    'privacy_models': [
                        {
                            'model_name': 'epsilon-delta-differential_privacy',
                            'model_params': {
                                'epsilon': eps,
                                'delta': 10E-12
                            }
                        }
                    ]
                }
                project = Project(name='test', description='demo', dataset_id=dataset.dataset_id,
                                  creator_id=user.user_id, researcher_id=user.user_id,
                                  filename='test.csv', config=config)
                project.save()

                jwt = login_user(self.client, user.email_address,
                                 password).get_json()['access_token']
                query = {'query': 'SELECT COUNT(*) FROM PRIORI'}
                response = self.client.post('/api/v1/projects/' + str(project.project_id) +
                                            '/analyze', json=query,
                                            headers=auth_header(jwt))
                assert response.status_code == 200
                results.append({'eps': eps, 'result': response.get_json()
                                ['query_result']['Col1']['0']})

        results_df = pd.DataFrame(results)
        file_dir = os.path.dirname(os.path.abspath(__file__))
        results_df.to_csv(os.path.join(file_dir, 'differential_privacy.csv'), index=False)
