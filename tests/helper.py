"""Contains frequently needed functionality."""

from mimesis import Generic
from mimesis.builtins import USASpecProvider
g = Generic('en')
g.add_provider(USASpecProvider)


def login_user(client, email_address, password):
    """Login request with provided login credentials.

    Args:
        client (flask.testing.FlaskClient): Test client.
        email_address (str): Email address.
        password (str): Password.

    Returns:
        Response: Response object

    """
    return client.post('/api/v1/login', json={
        'email_address': email_address, 'password': password})


def auth_header(token):
    """Create auth header with provided token.

    Args:
        token (str): JWT.

    Returns:
        dict: Authorization header.

    """
    return {'Authorization': 'Bearer ' + token}


def create_fake_rows_non_interactive(num=1):
    """Create rows with fake data for performance testing (non interactive scenario).

    Args:
        num (int, optional): Number of rows to create. Defaults to 1.

    Returns:
        list: List of rows.

    """
    return [{'ssn': g.usa_provider.ssn(),
             'zip_code': g.address.zip_code(),
             'age': g.person.age(minimum=0, maximum=99),
             'gender': g.person.gender(iso5218=True),
             'blood_type': g.person.blood_type()} for x in range(num)]


def create_fake_rows_interactive(num=1):
    """Create rows with fake data for performance testing (interactive scenario).

    Args:
        num (int, optional): Number of rows to create. Defaults to 1.

    Returns:
        list: List of rows.

    """
    return [{'zip_code': g.address.zip_code(),
             'age': g.person.age(minimum=0, maximum=100),
             'gender': g.person.gender(),
             'blood_type': g.person.blood_type()} for x in range(num)]
