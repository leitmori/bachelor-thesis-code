FROM python:3.8-slim-buster

RUN adduser --disabled-password --gecos "" priori
USER priori
WORKDIR /home/priori
COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt
COPY app app
COPY priori.py config.py boot.sh ./
ENV FLASK_APP priori.py

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]
